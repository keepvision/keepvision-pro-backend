"use strict";

var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var configShema = new Schema({
	key:String,
	value:String,
	lang:String	
});

module.exports = mongoose.model('Config', configShema, 'config');