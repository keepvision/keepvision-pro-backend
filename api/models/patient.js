'use strict';
var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var Patient = new Schema({
  promo_code: String,
  status: Number,
  doctor: {
    id: String,
    last_name: String,
    first_name: String,
    mid_name: String
  },
  clinic: {
    id: Schema.Types.ObjectId,
    name: String
  },
  country: {
    id: Schema.Types.ObjectId,
    name: String
  },
  city: {
    id: Schema.Types.ObjectId,
    name: String
  },
  last_name: String,
  first_name: String,
  mid_name: String,
  birthdate: Date,
  gender: Number,
  api_key: String,
  date_update: Date,
  GCM_token:String,
  lang:String,
  email:String,
  phone_number:String,
  weight:Number,
  height:Number
});

module.exports = mongoose.model('Patient', Patient, 'patient');
