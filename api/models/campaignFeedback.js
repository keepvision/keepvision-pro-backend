"use strict";

var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var campaignFeedbackShema = new Schema({
	patient:{
		id:Schema.Types.ObjectId
	},
	action_date:Date,
	action_type:Number,
	campaign:{
		id:Schema.Types.ObjectId
	}
});

module.exports = mongoose.model('CampaignFeedback', campaignFeedbackShema, 'campaign_feedback');