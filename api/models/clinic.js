"use strict";

var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var clinicShema = new Schema({
	name:String,
	country:{
		id:Schema.Types.ObjectId,
		name:String
	},
	city:{
		id:Schema.Types.ObjectId,
		name:String
	},
	paid_up:Date,
	status:Number,
	address:String,
	telephone:String
});

module.exports = mongoose.model('Clinic', clinicShema, 'clinic');