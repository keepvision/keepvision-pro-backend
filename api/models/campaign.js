"use strict";

var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var campaignSchema = new Schema({
  date_begin: Date,
  clinic:mongoose.Schema.Types.Mixed,
  status:Number,
  content: mongoose.Schema.Types.Mixed,
  stats:{
    count_action_read:Number,
    count_action_call:Number
  }
});

module.exports = mongoose.model('Campaign', campaignSchema, 'campaign');