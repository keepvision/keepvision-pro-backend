var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var AggregateData = new Schema({
  patient:{
    id: Schema.Types.ObjectId,
  },
  interval_type:Number,
 	interval_date:mongoose.Schema.Types.Mixed,
  interval_data:mongoose.Schema.Types.Mixed
});

module.exports = mongoose.model('AggregateData', AggregateData, 'aggregated_data');