var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var UsageSession = new Schema({
  patient:{
    id: Schema.Types.ObjectId,
  },
  date_begin:Date,
  date_end:Date,
  session_data:mongoose.Schema.Types.Mixed
});

module.exports = mongoose.model('UsageSession', UsageSession, 'usage_session');