'use strict';
var mongoose = require('mongoose');
var _ = require('underscore');
var url = require('url');
var qs = require('querystring');


function getIpAddress(req) {
  if (!req) {
    return;
  }
  return req.headers['x-forwarded-for'] ||
    req.connection.remoteAddress ||
    req.socket.remoteAddress;
  /* ||
       req.connection.socket.remoteAddress;*/
}

function getValue(req, value) {
  var query = url.parse(req.url).query,
    parsed = qs.parse(query);
  _.extend(value, req.paramsLog);
  _.extend(value, req.body);
  _.extend(value, parsed);
  if (req.hashStr) {
    value.hash_str = req.hashStr;
    value.body_request = JSON.stringify(req.body);
  }
}

var Schema = mongoose.Schema;

var EventSchema = new Schema({
  event_code: String,
  event_date: {
    type: Date,
    default: Date.now
  },
  is_error: Boolean,
  description: String,
  value: Object
});

EventSchema.methods.addLog = function(req, isError, description) {
  var self = this,
    value = {};

  // if (req.application) {
  //   value.appName = req.application.name;
  // }
  value.ip = getIpAddress(req);
  getValue(req, value);

  self.event_code = req.log_code;
  self.is_error = isError;
  self.description = description;
  self.value = value;
  self.save();
};


module.exports = mongoose.model('Event_log', EventSchema, 'event_log');
