var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var Diagnosis = new Schema({
 patient:{
    id: Schema.Types.ObjectId,
 },
 date_update:Date,
 diagnosis_data:mongoose.Schema.Types.Mixed
});

module.exports = mongoose.model('Diagnosis', Diagnosis, 'diagnosis');