'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var ConfigModel = require('./config');
var EventModel = require('./event_log');
var _ = require('underscore');
var constants = require('../libs/constants');

var messageSchema = new Schema({
  date_sent: Date,
  patient: {
    id: Schema.Types.ObjectId,
    first_name: String,
    last_name: String,
    mid_name: String,
    GCM_token:String
  },
  sender: {
    picture_url: String,
    name: String
  },
  content: {
    title: String,
    text: String,
    link:String,
    text2:String,
    link2:String,
    date:Date,
    type:Number,
    imageUrl: String,
    call_phone: String
  },
  status: Number
});

// messageSchema.methods.addNew = function(patient, sender, content, dateSent) {
//   this.date_sent = dateSent || new Date();
//   this.sender = sender;
//   this.patient = patient;
//   this.content = content;
//   this.save(function(err) {
//     if (err) {
//       (new EventModel()).addLog(req, true, 'Ошибка при создании новой записи в message:' + err);
//     }
//   });
// };
function getValueByKey(configs, key) {
  var obj = _.find(configs, function(config) {
    return config.key === key;
  });
  return obj && obj.value;
}

messageSchema.methods.addMessageHello = function(req) {
  var self = this;
  ConfigModel.find({
      $or: [{
        key: 'project_name'
      }, {
        key: 'project_picture_url'
      }, {
        key: 'hello_msg_body'
      }, {
        key: 'hello_msg_title'
      }, {
        key: 'hello_msg_picture_url'
      }]
    },
    function(err, configs) {
      self.date_sent = new Date();
      self.sender = {
        name: getValueByKey(configs, 'project_name'),
        picture_url: getValueByKey(configs, 'project_picture_url')
      };
      self.patient = req.patient;
      self.patient.id = req.patient._id;
      self.content = {
        text: getValueByKey(configs, 'hello_msg_body'),
        title: getValueByKey(configs, 'hello_msg_title'),
        imageUrl: getValueByKey(configs, 'hello_msg_picture_url'),
        date:new Date(),
        type:constants.MESSAGE_TYPE_PROMO_CODE_ACTIVATED
      };
      self.status = constants.MESSAGE_STATUS_NOTREAD;
      self.save(function(err) {
        if (err) {
          (new EventModel()).addLog(req, true, 'Ошибка при создании новой записи в message:' + err);
        }
      });
    });
};

messageSchema.methods.addMessageDiagnosis = function(req) {
  var self = this;
  ConfigModel.find({
      $or: [{
        key: 'project_name'
      }, {
        key: 'project_picture_url'
      }, {
        key: 'diagnosis_msg_body'
      }, {
        key: 'disgnosis_msg_title'
      },{
        key: 'disgnosis_msg_picture_url'
      }]
    },
    function(err, configs) {
      self.date_sent = new Date();
      self.sender = {
        name: getValueByKey(configs, 'project_name'),
        picture_url: getValueByKey(configs, 'project_picture_url')
      };
      self.patient = req.patient;
      self.patient.id = req.patient._id;

      self.content = {
        text: getValueByKey(configs, 'diagnosis_msg_body'),
        title: getValueByKey(configs, 'disgnosis_msg_title'),
        imageUrl: getValueByKey(configs, 'disgnosis_msg_picture_url'),
        date:new Date(),
        type:constants.MESSAGE_TYPE_TEST_RESULT
      };

      self.status = constants.MESSAGE_STATUS_NOTREAD;

      self.save(function(err) {
        if (err) {
          (new EventModel()).addLog(req, true, 'Ошибка при создании новой записи в message:' + err);
        }
      });
    });
};

module.exports = mongoose.model('Message', messageSchema, 'message');
