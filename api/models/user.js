var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var User = new Schema({
  _id: {
    type: String
  },
  emails: [
    {
      address:  String
    }
  ],
  services:{
    password:{
      bcrypt:String
    }
  },
  stats:{
    count_patient:Number,
    count_promo_code:Number
  }
});

module.exports = mongoose.model('Users', User, 'users');