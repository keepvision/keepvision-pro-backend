'use strict';
var validator = require('validator');
var _ = require('underscore');
var constants = require('../libs/constants');
var EventModel = require('../models/event_log');
var PatientModel = require('../models/patient');

exports.get = function(req, res, next) {
  var patient = _.pick(req.patient, 'status', 'last_name', 'first_name',
    'mid_name', 'birthdate', 'gender', 'clinic', 'doctor', 'country', 'city', 
    'GCM_token', 'lang', 'email', 'phone_number', 'weight', 'height' );
  patient.id = req.patient._id;

  (new EventModel()).addLog(req, false, 'Данные пациента успешно получены');
  res.send(patient);
};

exports.put = function(req, res, next) {
  var lastName = req.body.last_name,
    firstName = req.body.first_name,
    midName = req.body.mid_name,
    gender = req.body.gender,
    birthdate = req.body.birthdate,
    GCMToken = req.body.GCM_token,
    lang = req.body.lang,
    email = req.body.email,
    phoneNumber = req.body.phone_number,
    weight = req.body.weight,
    height = req.body.height,
    patient = req.patient,
    error;

  if (lastName) {
    if (!validator.isLength(lastName, 0, 255)) {
      error = new Error('Некорректная длинна фамилии');
      error.status = 400;
      return next(error);
    }
    patient.last_name = lastName;
  }

  if (firstName) {
    if (!validator.isLength(firstName, 0, 255)) {
      error = new Error('Некорректная длинна именни');
      error.status = 400;
      return next(error);
    }
    patient.first_name = firstName;
  }

  if (midName) {
    if (!validator.isLength(midName, 0, 255)) {
      error = new Error('Некорректная длинна отчества');
      error.status = 400;
      return next(error);
    }
    patient.mid_name = midName;
  }

  if (gender !== undefined) {
    if (!(gender in [constants.PATIENT_GENDER_FEMALE, constants.PATIENT_GENDER_MALE])) {
      error = new Error('Некорректное значение поля gender');
      error.status = 400;
      return next(error);
    }
    patient.gender = gender;
  }

  if (birthdate) {
    if (!validator.isDate(birthdate)) {
      error = new Error('Некорректное значение даты рождения');
      error.status = 400;
      return next(error);
    }
    patient.birthdate = birthdate;
  }

  if (GCMToken) {
    patient.GCM_token = GCMToken;
  }

  if (lang) {
    patient.lang = lang;
  }

  if (email) {
    if (!validator.isEmail(email)) {
      error = new Error('Некорректное значение email');
      error.status = 400;
      return next(error);
    }
    patient.email = email;
  }

  if (phoneNumber) {
    if (!validator.isLength(phoneNumber, 0, 100)) {
      error = new Error('Некорректная длинна номер телефона');
      error.status = 400;
      return next(error);
    }
    patient.phone_number = phoneNumber;
  }


  if (weight) {
    if (isNaN(weight)) {
      error = new Error('Вес должен быть числом');
      error.status = 400;
      return next(error);
    }
    patient.weight = weight;
  }

  if (height) {
    if (isNaN(height)) {
      error = new Error('Рост должен быть числом');
      error.status = 400;
      return next(error);
    }
    patient.height = height;
  }



  patient.save(function(err){
    if (err) {
      return next(err);
    }
    (new EventModel()).addLog(req, false, 'Данные пациента успешно обновлены');
    res.status(204).send();
  });
};
