'use strict';
var mongoose = require('../libs/mongoose').mongoose,
checkSign = require('../libs/checkSign'),
patientMiddleware = require('../middleware/patient'),
campaignMiddleware = require('../middleware/campaign'),
util = require('../libs/util');

module.exports = function(app) {
  app.get('/api/v2/actions/get_api_key', 
    util.setLogCode('api01'),
    patientMiddleware.findByPromoCode,
    require('./getApiKey').get);
  app.get('/api/v2/patients/:patient_id',
    util.setLogCode('api02'),
    patientMiddleware.find,
    checkSign,
    require('./patient').get);
  app.put('/api/v2/patients/:patient_id',
    util.setLogCode('api03'),
    patientMiddleware.find,
    checkSign,
    require('./patient').put);
  app.get('/api/v2/patients/:patient_id/messages',
    util.setLogCode('api04'),
    patientMiddleware.find,
    checkSign,
    require('./messages').get);
  app.post('/api/v2/patients/:patient_id/usage_sessions',
    util.setLogCode('api05'),
    patientMiddleware.find,
    checkSign,
    require('./usageSession').post);
  app.post('/api/v2/patients/:patient_id/diagnosis',
    util.setLogCode('api06'),
    patientMiddleware.find,
    checkSign,
    require('./diagnosis').post);
  app.get('/api/v2/patients/:patient_id/intervals',
    util.setLogCode('api07'),
  	patientMiddleware.find,
    checkSign,
  	require('./intervals').get);
  app.get('/api/v2/patients/:patient_id/factors',
  	util.setLogCode('api08'),
    patientMiddleware.find,
    checkSign,
    require('./factors').get);
  app.post('/api/v2/patients/:patient_id/campaign_feedbacks',
    util.setLogCode('api09'),
    patientMiddleware.find,
    checkSign,
    campaignMiddleware.find,
    require('./campaignFeedback').post);
};
