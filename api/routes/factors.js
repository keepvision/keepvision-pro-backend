'use strict';
var EventModel = require('../models/event_log');
var FactorMatrix = require('../models/factorMatrix').FactorMatrix,
  util = require('../libs/util');

exports.get = function(req, res, next) {
  var age = util.getAge(req.patient.birthdate);
  FactorMatrix.find({
    age_from: {
      $lte: age
    },
    age_to: {
      $gte: age
    }
  }, function(err, factors) {
    if (err) {
      return next(err);
    }
    (new EventModel()).addLog(req, false, 'матрица факторов получена');
    res.send(factors);
  });
};
