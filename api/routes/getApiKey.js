"use strict";
var constants = require('../libs/constants');
var EventModel = require('../models/event_log');
var MessageModel = require('../models/message');
var updateDoctorStat = require('../libs/updateDoctorStat');

// Получение API_key
exports.get = function(req, res, next) {
  var patient = req.patient;
  // patient.promo_code = undefined; ToDo на время теста
  patient.date_update = new Date();
  patient.status = constants.PATIENT_STATUS_ACTIVE;
  patient.save(function(err) {
    if (err) {
      (new EventModel()).addLog(req, true, 'Не удалось удалить промо код:' + err);
    } else {
      updateDoctorStat(req);
    }
  });

  (new EventModel()).addLog(req, false, 'api key успешно получен');
  (new MessageModel()).addMessageHello(req);
  return res.status(200).send({
    "patient_id": patient._id,
    "api_key": patient.api_key
  });
};
