'use strict';
var validator = require('validator');
var DiagnosisModel = require('../models/diagnosis');
var EventModel = require('../models/event_log');
var MessageModel = require('../models/message');

exports.post = function(req, res, next) {
  var diagnosis = new DiagnosisModel(),
    dateUpdate = req.body.date_update,
    error;

  if (!dateUpdate) {
    error = new Error('Не указан обязательный параметр date_update');
    error.status = 400;
    return next(error);
  }

  if (!validator.isDate(dateUpdate)) {
    error = new Error('Некорректное значение date_update');
    error.status = 400;
    return next(error);
  }

  diagnosis.date_update = dateUpdate;
  diagnosis.diagnosis_data = req.body.diagnosis_data;
  diagnosis.patient = {
    id: req.patient._id
  }; 
  diagnosis.save(function(err) {
    if (err) {
      return next(err);
    }
    (new EventModel()).addLog(req, false, 'Данные диагностики успешно сохранены');
    (new MessageModel()).addMessageDiagnosis(req);
    res.status(201).send(diagnosis);
  });
};
