'use strict';
var url = require('url');
var qs = require('querystring');
var validator = require('validator');
var MessageModel = require('../models/message');
var EventModel = require('../models/event_log');
var constants = require('../libs/constants');

exports.get = function(req, res, next) {
  var query = url.parse(req.url).query,
    parsed = qs.parse(query),
    patient = req.patient,
    conditions = {
      'patient.id': patient._id
    },
    error;

  if (parsed.after_date) {
    if (!validator.isDate(parsed.after_date)) {
      error = new Error('after_date должен быть датой');
      error.status = 400;
      return next(error);
    }
    conditions.date_sent = {
      '$gte': parsed.after_date
    };
  }

  if (parsed.before_date) {
    if (!validator.isDate(parsed.before_date)) {
      error = new Error('before_date должен быть датой');
      error.status = 400;
      return next(error);
    }
    conditions.date_sent = {
      '$lte': parsed.before_date
    };
  }

  MessageModel.find(conditions, function(err, messages) {
    if (err) {
      return next(err);
    }
    (new EventModel()).addLog(req, false, 'Сообщения успешно получены');
    res.send(messages);
  });
};

exports.post = function(req, res, next) {
  var message = new MessageModel(),
  patient = req.body.patient,
  sender = req.body.sender,
  dateSent = req.body.date_sent,
  content = req.body.content;

 // ToDo Добавить проверки
  message.date_sent = dateSent;
  message.patient = patient;
  message.sender = sender;
  message.content = message.content;
  message.status = constants.MESSAGE_STATUS_NOTREAD;

  message.save(function(err) {
    if (err) {
      return next(err);
    }
    (new EventModel()).addLog(req, false, 'Сообщения успешно сохранено');
    res.status(204).send();
  });
}
