// "use strict";

// var url = require('url');
// var qs = require('querystring');
// var bcrypt = require('bcrypt-nodejs');
// var crypto = require('crypto');
// var UserModel = require('../models/user').User;
// var ChildModel = require('../models/child').Child;
// var UsageSession = require('../models/usageSession').UsageSession;
// var Diagnosis = require('../models/diagnosis').Diagnosis;
// var IntervalMatrix = require('../models/intervalMatrix').IntervalMatrix;
// var FactorMatrix = require('../models/factorMatrix').FactorMatrix;
// var log = require('../libs/log')(module);
// var REST_NAME;
// var textMessage = require('../libs/textMessage');
// var constants = require('../libs/constants');
// var config = require('../config/config');

// // Сообщение о событии в приложении ребёнка
// exports.post = function(req, res) {
//   var query = url.parse(req.url).query,
//   parsed = qs.parse(query),
//   security_token = parsed.security_token,
//   childId = req.params.child_id,
//   eventCode = req.body.event_code,
//   eventOptions = req.body.event_options;
//   REST_NAME = new Date + '/api/v1/children/' + childId + '/events: ';
//   if (!security_token) {
//     log.error(REST_NAME + 'Incorrect arguments: security_token required');
//     return res.status(400).send('Incorrect arguments: security_token required');
//   }
//   if (typeof eventCode !== "number") {
//     log.error(REST_NAME + 'Incorrect arguments: event_code required');
//     return res.status(400).send('Incorrect arguments: event_code required');
//   }

//   ChildModel.findById(childId, function(err, child) {
//   	if (!child) {
//   		log.error(REST_NAME + 'Child not found:' + childId);
//       return res.status(404).send('Child not found');
//     }
//     if (err) {
//       log.error(REST_NAME + 'Server failed:' + err);
//       return res.status(501).send('Server failed');
//     }

//     var userId = child.user.id;
//     UserModel.findById(userId, 'api_key', function(err, user) {
//       if (!user) {
//         log.error(REST_NAME + 'User not found');
//         return res.status(501).send('User not found');
//       }
//       if (err) {
//         log.error(REST_NAME + 'Server failed:' + err);
//         return res.status(501).send('Server failed');
//       }
      
//       if (!checkHash(childId, eventCode, eventOptions, user.api_key, security_token)) {
//         log.error(REST_NAME + 'Authorization failed: incorrect security_token');
//         return res.status(403).send('Authorization failed: incorrect security_token');
//       }

//       switch(eventCode) {
//         case constants.EVENT_CODE_TIME_TRACKING: 
//           tickTimeTracking(child, userId, eventOptions, res);
//           break;
//         case constants.EVENT_CODE_DIAGNOSIS:
//           saveDiagnosis(eventOptions, childId, userId, child, res);
//           break;
//         default:
//           log.error(REST_NAME + 'Incorrect arguments: event_code');
//            return res.status(400).send('Incorrect arguments: event_code');
//           break;
//       }
//     });
//   });
// }

// /**
//  * checkHash() Считает Хэш для аргументов и сравнивает его с токеном.
//  *  При совпадении возвращает истину, иначе ложь.
//  *
//  * @param <String> childId
//  * @param <Number> eventCode
//  * @param <Object> eventOptions
//  * @param <String> api_key
//  * @param <String> security_token
//  */
// function checkHash(childId, eventCode, eventOptions, api_key, security_token) {
//       var paramStr = childId + eventCode;
//       for(var p in eventOptions) {
//         if (p === 'tests') {
//            eventOptions[p].forEach(function(elem) {
//             for(var key in elem) {
//               if (typeof elem[key] !== 'undefined' ) paramStr += elem[key];
//             }
//            });
//         }
//         else if (typeof eventOptions[p] !== 'undefined' ) paramStr += eventOptions[p];
//       }
//       paramStr += api_key;
//       if (crypto.createHash('sha256').update(paramStr).digest('hex') === security_token) {
//         return true;
//       } 
//       return false;
// }

// /**
//  * tickTimeTracking() изменяет значение в коллекции usage_session
//  * для трекинга времени
//  *
//  * @param <Object> child
//  * @param <String> userId
//  * @param <Object> eventOptions
//  * @param <Object> res
//  */
//  function tickTimeTracking(child, userId, eventOptions, res) {
//   // if (!child.birthday) {
//   //   log.error(REST_NAME + 'birthday is undefined');
//   //   return res.status(501).send('Server failed');
//   // }

//    if (!(eventOptions && eventOptions.start)) {
//     log.error(REST_NAME + 'event_options.start is undefined');
//     return res.status(400).send('event_options.start required');
//   }

//   var ms = Date.parse(eventOptions.start);
//   if (isNaN(ms)) {
//     log.error(REST_NAME + 'event_options.start is incorrect');
//     return res.status(400).send('event_options.start is incorrect');
//   }

//   if (child.last_usage_session_id) {
//     UsageSession.findById(child.last_usage_session_id, function(err, lastSession) {
//       if (err) {
//         log.error(err);
//         return res.status(501).send('Server failed');
//       }
//       if (!lastSession) {
//         log.error(REST_NAME + 'usage_session with id:' + child.last_usage_session_id + ' not found');
//         return res.status(501).send('Server failed');
//       }
     
//       if (lastSession.date_begin.getTime() ===  getDateBegin(eventOptions).getTime()) { 
//         //Продолжаем текущую сессию
//         findInterval(child, lastSession, userId, eventOptions, res);
//       } else { 
//         UsageSession.find({date_begin:eventOptions.start, "child.id":child._id}, function(err, session) {
//           if (session.length !== 0) {
//             //Обновляем найденную сессию
//             findInterval(child, session[0], userId, eventOptions, res);
//           }
//           else {
//             // Начинаем новую сессию
//             createNewSessionUsage(res, child, eventOptions, userId);
//           }
//         });
//       }

//     });
//   } else {
//     //Первая сессия у ребенка
//     createNewSessionUsage(res, child, eventOptions, userId); 
//   }
// }

// /**
//  * createNewSessionUsage() Создать новую сессию
//  * 
//  * @param <Object> res
//  * @param <Object> child
//  * @param <Object> eventOptions 
//  * @param <String> userId 
//  */
//  function createNewSessionUsage(res, child, eventOptions, userId ) {
//    var dataSession = {},
//    continue_using = eventOptions.continue_using?+eventOptions.continue_using:0,
//    interval = eventOptions.relax?eventOptions.relax:0;

//    dataSession.child = {};
//    dataSession.child.id = child._id;
//    dataSession.date_begin = getDateBegin(eventOptions);
//    dataSession.date_end = getDateEnd(eventOptions);
//    dataSession.time_zone = getTimeZone(eventOptions.start);
//    dataSession.duration = fromMsInMinute(continue_using);
//    dataSession.message_is_send = constants.MESSAGE_IS_SEND_SAFE;
//    dataSession.interval = fromMsInMinute(interval);
//    if ( typeof eventOptions.lighting_level !== "undefined" && +eventOptions.lighting_level !== -1) {
//      dataSession.lighting_level = +eventOptions.lighting_level;
//      dataSession.lighting_count = 1;
//    }
//    //if (!dataSession.log) dataSession.log = [];
//   // dataSession.log.push(eventOptions);

//    var usageSession = new UsageSession(dataSession);
//    usageSession.save(function(err, session) {
//     if (err) {
//       log.error(REST_NAME + err);
//       return res.status(501).send('Server failed');
//     }
//     ChildModel.findByIdAndUpdate(child._id, {last_usage_session_id:session._id}, function(err) {
//       if (err) log.error(REST_NAME + err);
//       findInterval(child, session, userId, eventOptions, res);
//      // return res.status(201).send({'zone': constants.COLOR_GREEN});
//     });
//   });
// }

// /**
//  * saveDiagnosis() Сохранить диагноз 
//  * 
//  * @param <Object> eventOptions
//  * @param <Object> childId
//  * @param <Object> res
//  */
// function  saveDiagnosis(eventOptions, childId, userId, child, res) {
//     var diagnosisData = {};
//     for(var p in eventOptions) {
//         if (typeof eventOptions[p] !== 'undefined') diagnosisData[p] = eventOptions[p];
//     }
//     var data = {};
//     data.child = {};
//     data.child.id = childId;
//     if (eventOptions.date_of_test) data.date_update = new Date(eventOptions.date_of_test);
//     else data.date_update = new Date();
//     data.diagnosis_data = diagnosisData;
//     data.vision_safety = calculateVisionSafety(diagnosisData.acuity);

//     var diagnosis =  new Diagnosis(data);
//     diagnosis.save(function(err){
//       if (err) {
//         log.error(REST_NAME + err);
//         return res.status(501).send('Server failed');
//       }
//       var text = config.get('messageDiagnos:text');  
//       var title = child.name;
//       createPushMessage(userId, 
//                       title, 
//                       text,
//                       constants.MESSAGE_SUBTYPE_DIAGNOS,
//                       childId,
//                       data.date_update,
//                       diagnosis._id);
//       return res.status(201).send('OK');
//     });
// }

// /**
//  * calculateVisionSafety() Рассчитать сохранность зрения
//  * 
//  * @param <Number> acuity острота зрения
//  */
// function calculateVisionSafety(acuity) {
//   if (!acuity) return;
//   var table = constants.VISUAL_ACUITY_TABLE,
//   currLine;
//   for(var i = 0; i < table.length; i++) {
//     if ( acuity <= table[i].acuity) {
//       currLine = i;
//       break;
//     }
//   }
//   if (typeof currLine === 'undefined' ) return;

  
//   if (currLine === 0 ) { 
//     return table[currLine];
//   }
//   else if ( currLine < table.length) 
//   {
//     var prevLine = currLine - 1,
//     a = table[currLine].vision - table[prevLine].vision,
//     b = table[currLine].acuity - table[prevLine].acuity,
//     c = acuity - table[prevLine].acuity;
//     if (b !== 0) return a * c / b + table[prevLine].vision;
//   }
// }


// /**
//  * findInterval() Найти интервал в матрице интервалов
//  * 
//  * @param <Object> child ребенок
//  * @param <Object> currentSession текущая(обновляемая) сессия
//  * @param <String> userId id пользователя
//  * @param <Object> eventOptions
//  * @param <Object> res
//  */
// function findInterval(child, currentSession, userId, eventOptions, res) { 
//   // if (!child.birthday) {
//   //   log.error(REST_NAME + 'birthday is undefined');
//   //   return res.status(501).send('Server failed');
//   // }

//   var age = getAgeChild(child.birthday),
//   dateBeginSession = getDateBegin(eventOptions);
//   IntervalMatrix.findOne({age_from:{$lte:age}, age_to:{$gte:age}}, function(err, interval) {
//     if (err) {
//       log.error(REST_NAME + err);
//       return res.status(501).send('Server failed');
//     }
//     if (!interval) {
//       log.error(REST_NAME + 'interval not found');
//       return res.status(501).send('Server failed');
//     }
//     var dateBeginPrevDay = prevDay(dateBeginSession);
//     //Пока без учета перехода сессии с одного дня на другой
//     UsageSession.find({date_begin:{ 
//       "$gte": dateBeginPrevDay,
//       "$lt": dateBeginSession
//     },"child.id":child._id}, function(err, session) {

//       var durationUseDay = fromMsInMinute(+eventOptions.continue_using);
//       session.forEach(function(oneSession) { 
//         durationUseDay += oneSession.duration;
//       }); 
//       calculateCoefficient(currentSession, age, child, durationUseDay, interval, userId, eventOptions, res);  
//     });
//   });
// }

// /**
//  * calculateCoefficient() Рассчитать коэффициент в матрице коэффициентов
//  * 
//  * @param <Object> currentSession Обновляемая сессия
//  * @param <Number> age Возраст ребенка
//  * @param <Object> child ребенок
//  * @param <Number> durationUseDay Длительность использования за сутки
//  * @param <Object> interval Интервал из матрицы интервалов
//  * @param <String> userId id пользователя
//  * @param <Object> eventOptions
//  * @param <Object> res
//  */
// function calculateCoefficient(currentSession, age, child, durationUseDay, interval, userId, eventOptions,  res) {
//   FactorMatrix.find({}, function(err, docs) {
//     if (err) {
//       log.error(REST_NAME + err);
//       return res.status(501).send('Server failed');
//     }

//     var durationBreaksFactor = findFactorInMatrix(docs, constants.FACTOR_MATRIX_DURATION_BREAKS_CODE, age, currentSession.interval),
//     durationUseFactor = findFactorInMatrix(docs, constants.FACTOR_MATRIX_DURATION_USE_CODE, age, durationUseDay);
//     if (child.options && child.options.sharpness) {
//       var sharpnessFactor = findFactorInMatrix(docs, constants.FACTOR_MATRIX_SHARPNESS_VISION_CODE, age, child.options.sharpness);
//     }
//     else sharpnessFactor = 1
//     if (child.gender) {
//       var genderFactor = findFactorInMatrix(docs, constants.FACTOR_MATRIX_GENDER_CODE, age, child.gender);
//     }
//     else genderFactor = 1;
//     if (child.options && child.options.parents_desease_vision) {
//       var heredityFactor = findFactorInMatrix(docs, constants.FACTOR_MATRIX_HEREDITY_CODE, age, child.options.parents_desease_vision);
//     }
//     else heredityFactor = 1;

//     if (currentSession.lighting_level && currentSession.lighting_count) {
//       var lighting = currentSession.lighting_level / currentSession.lighting_count;
//       var lightingFactor = findFactorInMatrix(docs, constants.FACTOR_MATRIX_LIGHTING_CODE, age, child.gender);
//     }
//     else  lightingFactor = 1;
   
//     var coefficientObj = {
//       durationBreaksFactor:durationBreaksFactor,
//       durationUseFactor:durationUseFactor,
//       sharpnessFactor:sharpnessFactor,
//       genderFactor:genderFactor,
//       heredityFactor:heredityFactor,
//       lightingFactor:lightingFactor
//     }
   
//     updateSession(interval, coefficientObj, eventOptions, currentSession, userId, child, res);
//   });
// }

// /**
//  * updateSession() обновить сессию и отправить сообщение
//  * 
//  * @param <Object> interval Интервал из матрицы интервалов
//  * @param <Object> coefficientObj  коэффициенты рассчитанные на основе матрицы коэффициентов
//  * @param <Object> eventOptions
//  * @param <Object> currentSession Обновляемая сессия
//  * @param <String> userId id пользователя
//  * @param <Object> child данные о ребенке
//  * @param <Object> res
//  */
// function updateSession(interval, coefficientObj, eventOptions, currentSession, userId, child, res ) {
//   var MS_IN_MINUTE = 60000;
//   var duration = fromMsInMinute(+eventOptions.continue_using),
//   relax = eventOptions.relax?eventOptions.relax:0,
//   colorZone,
//   dataUpdate = {
//     date_end: getDateEnd(eventOptions), 
//     duration:duration,
//     time_zone:getTimeZone(eventOptions.start)
//   };

//   if (!currentSession.log) dataUpdate.log = [];
//   else dataUpdate.log = currentSession.log;

//   var coefficient =  coefficientObj.durationBreaksFactor *coefficientObj. durationUseFactor * 
//                     coefficientObj.sharpnessFactor * coefficientObj.genderFactor * 
//                     coefficientObj.heredityFactor * coefficientObj.lightingFactor;
//  coefficientObj.coefficient = coefficient;


//   colorZone = constants.COLOR_GREEN;
//   if (currentSession.date_yellow) colorZone = constants.COLOR_YELLOW;
//   if (currentSession.date_red) colorZone = constants.COLOR_RED;
//   if (!currentSession.date_yellow && interval.interval_yellow * coefficient < duration ) {
//     colorZone = constants.COLOR_YELLOW;
//     dataUpdate.date_yellow = new Date(+currentSession.date_begin + interval.interval_yellow * coefficient * MS_IN_MINUTE);
//     var objLog = {
//       interval:interval,
//       coefficientObj:coefficientObj,
//       duration:duration,
//       color:colorZone
//     };
//     dataUpdate.log.push(objLog);
//   }
//   if (!currentSession.date_red && interval.interval_red * coefficient < duration ) {
//     colorZone = constants.COLOR_RED;
//     dataUpdate.date_red = new Date(+currentSession.date_begin + interval.interval_red * coefficient * MS_IN_MINUTE);
//     var objLog = {
//       interval:interval,
//       coefficientObj:coefficientObj,
//       duration:duration,
//       color:colorZone
//     };
//     dataUpdate.log.push(objLog);
//   }

//   if ( typeof eventOptions.lighting_level !== "undefined" && +eventOptions.lighting_level !== -1) {
//      if (currentSession.lighting_level) {
//       dataUpdate.lighting_level = +currentSession.lighting_level + +eventOptions.lighting_level;
//      }
//      else  dataUpdate.lighting_level = eventOptions.lighting_level;

//      if (currentSession.lighting_count) {
//       dataUpdate.lighting_count = currentSession.lighting_count + 1;
//      }
//      else  dataUpdate.lighting_count = 1;
//    }

//   if (relax) dataUpdate.interval = fromMsInMinute(relax);

//   if (colorZone  === constants.COLOR_RED && currentSession.message_is_send !== constants.MESSAGE_IS_SEND_RED) {
//     dataUpdate.message_is_send = constants.MESSAGE_IS_SEND_RED;
//     createPushMessage(userId, 
//                       child.name, 
//                       config.get('messageRed:text'),
//                       constants.MESSAGE_SUBTYPE_TIME_TRACK,
//                       child._id,
//                       dataUpdate.date_end);
//   }

//   dataUpdate.log.push(eventOptions);
//   if (currentSession.date_end <= dataUpdate.date_end) {
//     UsageSession.findByIdAndUpdate(currentSession._id, dataUpdate, function(err) {
//       if (err) {
//         log.error(err);
//         return res.status(501).send('Server failed');
//       }
//       return res.status(201).send({'zone': colorZone});
//     });
//   }
//   else  return res.status(201).send({'zone': colorZone});
// }


// /**
//  * getAgeChild() Возвращает возраст ребенка на текущий момент
//  * 
//  * @param <Date> birthday День рождения ребенка
//  */
// function getAgeChild(birthday) {
//   if (!birthday) return 0;
//   var ageDifMs = Date.now() - birthday,
//   ageDate = new Date(ageDifMs);
//   ageDate.getUTCFullYear();
//   return Math.abs(ageDate.getUTCFullYear() - 1970);
// }

// /**
//  * createPushMessage() Создать Push-уведомление в коллекции push_message
//  * 
//  * @param <String> userId Id пользователя
//  * @param <String> title Заголовок уведомления 
//  * @param <String> text Текст уведомления
//  * @param <Integer> subtype подтип сообщения
//  * @param <String> ChildId Id ребенка
//  * @param <Object> date дата отчета
//  */
// function createPushMessage(userId, title, text, subtype, childId, date, diagnosisId) { 
//   var PushMessage = require('../models/pushMessage').PushMessage;
//   var dataPushMessage = {
//     user:{
//       id:userId
//     },
//     title:title,
//     text:text,
//     status:constants.PUSH_MESSAGE_CREATE,
//     childId: childId,
//     date:date,
//     type:constants.MESSAGE_TYPE_PUSH,
//     subtype:subtype
//   };
//   if (diagnosisId) dataPushMessage.diagnosisId = diagnosisId;
//   var message = new PushMessage(dataPushMessage);
//   message.save(function(err) {
//     if (err)  log.error(REST_NAME + err);
//   });
// }

// /**
//  * fromMsInMinute() Преобразование из миллисекунд в минуты
//  * 
//  * @param <Number> ms Время в миллисекунд
//  * @return <Number> время в минутах
//  */
// function fromMsInMinute(ms) {
//   if (!ms) return 0;
//   var MS_IN_MINUTE = 60000;
//   return Math.round(ms / MS_IN_MINUTE);
// }


// /**
//  * findFactorInMatrix() поиск коэффициента в матрице коэффициентов
//  * 
//  * @param <Object> matrix матрица коэффициентов
//  * @param <Number> code код коэффициента
//  * @param <Number> age Возраст ребенка
//  * @param <Number> value Значени аргумента
//  * @return <Number> время в минутах
//  */
// function findFactorInMatrix(matrix, code, age, value ) {
//   var factor = 1;
//   matrix.forEach(function(line) {
//     if ((line.code === code) &&
//        (line.age_from <= age) && ( age <= line.age_to) &&
//        (line.value_from <= value) && ( value <= line.value_to)) {
//       factor = line.factor / 100;
//     }
//   });
//   return factor;
// }


// /**
//  * prevDay() Получить предыдущий день
//  * 
//  * @param <Date> date 
//  * @return <Date> предыдущий день
//  */
// function prevDay(date) {
//   if (!date) return;
//   var day = date.getDate(),
//   yesterday = new Date(date);
//   yesterday.setDate( day - 1 );
//   return yesterday;
// }


// function getDateBegin(eventOptions) {
//   return new Date(eventOptions.start);
// }

// function getDateEnd(eventOptions) {
//   var  continue_using = eventOptions.continue_using?+eventOptions.continue_using:0;
//   return new Date(getDateBegin(eventOptions).getTime() + continue_using);
// }

// function getTimeZone(dateISO){
// 	  //return -(new Date(eventOptions.start).getTimezoneOffset()) / 60;
//     var len = dateISO.length - 3;
//     if (dateISO[len] === ':') len--;// var timeZone = +(dateISO[len-2] + dateISO[len-1]);
//    // else  var timeZone = +(dateISO[len-1] + dateISO[len]);
//    var timeZone = +(dateISO[len-2] + dateISO[len-1] + dateISO[len]);
//     if (isNaN(timeZone)) return 0;
//     else return timeZone;
// }