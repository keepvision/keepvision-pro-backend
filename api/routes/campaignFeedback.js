'use strict';
var validator = require('validator');
var CampaignFeedbackModel = require('../models/campaignFeedback');
var EventModel = require('../models/event_log');
var constants = require('../libs/constants');
var updateCampaignStat = require('../libs/updateCampaignStat');

exports.post = function(req, res, next) {
  var campaignFeedback = new CampaignFeedbackModel(),
    actionDate = req.body.action_date,
    actionType = req.body.action_type,
    error;

  if (!actionDate) {
    error = new Error('Не указан обязательный параметр action_update');
    error.status = 400;
    return next(error);
  }

  if (!validator.isDate(actionDate)) {
    error = new Error('Некорректное значение actionDate');
    error.status = 400;
    return next(error);
  }

  if (actionType === undefined || actionType === null) {
    error = new Error('Не указан обязательный параметр action_type');
    error.status = 400;
    return next(error);
  }

  if (!(actionType in [  constants.CAMPAIGN_ACTION_READ, constants.CAMPAIGN_ACTION_CALL])) {
    error = new Error('Некорректное значение action_type');
    error.status = 400;
    return next(error);
  }

  campaignFeedback.action_date = actionDate;
  campaignFeedback.action_type = actionType;
  campaignFeedback.campaign = {
    id:req.body.campaign.id
  };
  campaignFeedback.patient = {
    id: req.patient._id
  };
  campaignFeedback.save(function(err) {
    if (err) {
      return next(err);
    }
    updateCampaignStat(req);
    (new EventModel()).addLog(req, false, 'Данные диагностики успешно сохранены');
    res.status(201).send(campaignFeedback);
  });
};
