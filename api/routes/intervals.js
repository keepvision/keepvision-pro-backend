'use strict';
var EventModel = require('../models/event_log');
var IntervalMatrix = require('../models/intervalMatrix').IntervalMatrix,
util = require('../libs/util');

exports.get = function(req, res, next) {
  var age = util.getAge(req.patient.birthdate);
  IntervalMatrix.find({
    age_from: {
      $lte: age
    },
    age_to: {
      $gte: age
    }
  }, function(err, intervals) {
    if (err) {
      return next(err);
    }
    (new EventModel()).addLog(req, false, 'Матрица интервалов успешно получена');
    res.send(intervals);
  });
};
