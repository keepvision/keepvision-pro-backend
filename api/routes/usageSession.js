'use strict';
var validator = require('validator');
var UsageSessionModel = require('../models/usageSession');
var EventModel = require('../models/event_log');
var aggregated = require('../libs/aggregated');

exports.post = function(req, res, next) {
  var usageSession = new UsageSessionModel(),
    dateBegin = req.body.date_begin,
    error;

  if (!dateBegin) {
    error = new Error('Не указан обязательный параметр date_begin');
    error.status = 400;
    return next(error);
  }

  if (!validator.isDate(dateBegin)) {
    error = new Error('Некорректное значение даты начала сессии');
    error.status = 400;
    return next(error);
  }

  usageSession.date_begin = dateBegin;
  usageSession.session_data = req.body.session_data;
  usageSession.patient = {
    id: req.patient._id
  };
  usageSession.save(function(err) {
    if (err) {
      return next(err);
    }

    (new EventModel()).addLog(req, false, 'Данные сессии успешно сохранены');
    res.status(201).send(usageSession);
    aggregated(req);
  });
};
