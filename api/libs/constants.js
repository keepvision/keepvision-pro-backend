'use strict';
module.exports = Object.freeze({
	PROMOCOD_LENGTH:8,
	PATIENT_GENDER_FEMALE:0,	//женский пол
	PATIENT_GENDER_MALE:1,	//мужской пол
  PATIENT_STATUS_NOTACTIVATED: 0, //не активирован
  PATIENT_STATUS_ACTIVE: 1, //действующий пациент
  PATIENT_STATUS_STOPPED: 2, //пациент временно заблокирован
  PATIENT_STATUS_REMOVED: 3, //пациент/промо-код удалён
  CLINIC_STATUS_NEW:0,	//заявка на регистрацию, ожидающая одобрения администратора
	CLINIC_STATUS_ACTIVE:1,	//действующая клиника
	CLINIC_STATUS_STOPPED:2,	//клиника временно заблокирована
	CLINIC_STATUS_REMOVED:3,	//клиника/заявка удалена
	CAMPAIGN_ACTION_READ:0, //	пациент прочитал
	CAMPAIGN_ACTION_CALL:1,	// пациент позвонил
	MESSAGE_STATUS_NOTREAD:0,	//не прочитано
	MESSAGE_STATUS_READ:1,	//прочитано
	INTERVAL_TYPE_DAY:0,
	INTERVAL_TYPE_WEEK:1,
	INTERVAL_TYPE_MONTH:2,
	MESSAGE_TYPE_WELCOME:0,
	MESSAGE_TYPE_TEST_RESULT:1,
	MESSAGE_TYPE_PROMO_CODE_ACTIVATED:2,
	MESSAGE_TYPE_FILL_PROFILE:3,
	MESSAGE_TYPE_CAMPAIGN:4,
	MESSAGE_TYPE_INVITATION:5
});
