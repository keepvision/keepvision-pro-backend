'use strict';
var EventModel = require('../models/event_log');
var CampaignFeedback = require('../models/campaignFeedback');
var _ = require('underscore');
var constants = require('./constants');
/* 
  Обновить статистику рекламной кампании
*/
module.exports = function(req) {
  var campaign = req.campaign;
  CampaignFeedback.aggregate([{
      $match: {
        'campaign.id': campaign._id
      }
    }, {
      $group: {
        "_id": '$action_type',
        "count": {
          "$sum": 1
        }
      }
    }],
    function(err, results) {
      var actionReadStat,
        actionCallStat;

      if (err) {
        return (new EventModel()).addLog(req, true, 'Не удалось обновить статистику по рекламной кампании:' + err);
      }

      if (!campaign.stats) {
        campaign.stats = {};
      }

      actionReadStat = _.find(results, function(result) {
        return result._id === constants.CAMPAIGN_ACTION_READ;
      });

      actionCallStat = _.find(results, function(result) {
        return result._id === constants.CAMPAIGN_ACTION_CALL;
      });

      campaign.stats.count_action_read = actionReadStat ? actionReadStat.count : 0;
      campaign.stats.count_action_call = actionCallStat ? actionCallStat.count : 0;

      campaign.save(function(err) {
        if (err) {
          return (new EventModel()).addLog(req, true, 'Не удалось обновить статистику по рекламной кампании:' + err);
        }
      });
    });
};
