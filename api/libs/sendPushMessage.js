// 'use strict';
// var gcm = require('node-gcm');
// module.exports = function(message) {


//   // Create a message
//   // ... with default values
//   var message = new gcm.Message();

//   // ... or some given values
//   var message = new gcm.Message({
//     collapseKey: 'demo',
//     priority: 'high',
//     contentAvailable: true,
//     delayWhileIdle: true,
//     timeToLive: 3,
//     restrictedPackageName: "somePackageName",
//     dryRun: true,
//     // data: {
//     //   key1: 'message1',
//     //   key2: 'message2'
//     // },
//     notification: {
//       title: message.title,
//       // icon: "ic_launcher",
//       body: message.body
//     }
//   });

//   // Change the message data
//   // ... as key-value
//   // message.addData('key1', 'message1');
//   // message.addData('key2', 'message2');

//   // ... or as a data object (overwrites previous data object)
//   // message.addData({
//   //   key1: 'message1',
//   //   key2: 'message2'
//   // });

//   // Set up the sender with you API key
//   var sender = new gcm.Sender('insert Google Server API Key here');

//   // Add the registration tokens of the devices you want to send to
//   var registrationTokens = [];
//   registrationTokens.push(message.tokens);
//   // registrationTokens.push('regToken2');

//   // Send the message
//   // ... trying only once
//   sender.sendNoRetry(message, {
//     registrationTokens: registrationTokens
//   }, function(err, result) {
//     if (err) console.error(err);
//     else console.log(result);
//   });

//   // ... or retrying
//   sender.send(message, {
//     registrationTokens: registrationTokens
//   }, function(err, result) {
//     if (err) console.error(err);
//     else console.log(result);
//   });

//   // ... or retrying a specific number of times (10)
//   sender.send(message, {
//     registrationTokens: registrationTokens
//   }, 10, function(err, result) {
//     if (err) console.error(err);
//     else console.log(result);
//   });
// }
