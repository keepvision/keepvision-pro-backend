'use strict';
var EventModel = require('../models/event_log');
var PatientModel = require('../models/patient');
var UserModel = require('../models/user');
var _ = require('underscore');
var constants = require('./constants');
  /* 
  	Обновить статистику по врачу
  */
module.exports = function(req) {
  var doctor = req.patient.doctor;
  if (!(doctor && doctor.id)) {
    return;
  }
  var doctorId = doctor.id;
  UserModel.findById(doctorId, function(err, user) {
    if (err) {
      return (new EventModel()).addLog(req, true, 'Не удалось обновить статистику по врачу:' + err);
    }

    PatientModel.aggregate([{
        $match: {
          'doctor.id': doctorId
        }
      }, {
        $group: {
          "_id": '$status',
          "count": {
            "$sum": 1
          }
        }
      }],
      function(err, results) {
        var promoCodeStat,
        patientStat; 

        if (err) {
          return (new EventModel()).addLog(req, true, 'Не удалось обновить статистику по врачу:' + err);
        }

        if (!user.stats) {
          user.stats = {};
        }

        promoCodeStat = _.find(results, function(result) {
          return result._id === constants.PATIENT_STATUS_NOTACTIVATED;
        });

        patientStat = _.find(results, function(result) {
          return result._id === constants.PATIENT_STATUS_ACTIVE;
        });

        user.stats.count_promo_code  = promoCodeStat ? promoCodeStat.count : 0;
        user.stats.count_patient = patientStat ? patientStat.count : 0;

        user.save(function(err) {
          if (err) {
            return (new EventModel()).addLog(req, true, 'Не удалось обновить статистику по врачу:' + err);
          }
        });
      }
    );
  });
};
