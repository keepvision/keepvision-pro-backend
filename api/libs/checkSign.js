'use strict';
var url = require('url');
var qs = require('querystring');
var crypto = require('crypto');


function concatValueObj(obj) {
  var str = '';
  for (var key in obj) {
    if (key !== 'security_token') {
      if (typeof obj[key] === 'object') {
        str += concatValueObj(obj[key]);
      } else if (typeof obj[key] === 'array') {
        var array =  obj[key];
        for (var i = 0; i < array.length; i++) {
           str += concatValueObj(array[i]);
        };
      }
      else {
        str += obj[key];
      }
    }
  }
  return str;
}

//Проверяет корректность подписи
module.exports = function(req, res, next) {
  var query = url.parse(req.url).query;
  var parsed = qs.parse(query);
  var error;

  if (!parsed.security_token) {
    error = new Error('Нет API ключа для проверки!');
    error.status = 403;
    return next(error);
  }

  var hashStr = concatValueObj(req.params) + concatValueObj(parsed) + concatValueObj(req.body) + req.patient.api_key;
  if (parsed.security_token !== crypto.createHash('sha256').update(hashStr, 'utf8').digest('hex')) {
    req.hashStr = hashStr;
    error = new Error('Нет прав. Не пройдена проверка API_key');
    error.status = 403;
    return next(error);
  }
  return next();
};
