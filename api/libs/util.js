"use strict";

exports.setLogCode = function(log_code) {
  return function(req, res, next) {
    req.log_code = log_code;
    req.paramsLog = req.params;
    next();
  };
};

exports.getAge = function(birthday) {
  if (!birthday) {
    return 0;
  }
  var ageDifMs = Date.now() - birthday,
    ageDate = new Date(ageDifMs);
  ageDate.getUTCFullYear();
  var age = Math.abs(ageDate.getUTCFullYear() - 1970);
  return age;
};
