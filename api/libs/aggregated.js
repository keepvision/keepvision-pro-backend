'use strict';
var UsageSessionModel = require('../models/usageSession');
var AggregateDataModel = require('../models/aggregated_data');
var EventModel = require('../models/event_log');
var constants = require('./constants');
var moment = require('moment');
var async = require('async');

function getTimeZoneDiff(dateISO) {
  var timeZonePatient = moment.parseZone(dateISO).utcOffset();
  var currentTimeZone = -(new Date()).getTimezoneOffset();
  return timeZonePatient - currentTimeZone;
}

function getDatePatient(dateISO) {
  var date = moment(dateISO);
  date.add(getTimeZoneDiff(dateISO), 'minute');
  return date;
}

function getPeriod(dateISO) {
  var datePatient = getDatePatient(dateISO);
  var dateBegin = moment()
    .year(datePatient.year())
    .month(datePatient.month())
    .date(datePatient.date())
    .hour(0)
    .minute(0)
    .second(0);
  dateBegin.subtract(getTimeZoneDiff(dateISO), 'minute');
  var dateEnd = moment(dateBegin).add(1, 'day');
  return {
    begin: dateBegin.toDate(),
    end: dateEnd.toDate()
  };
}

function getDateObj(dateISO) {
  var date = getDatePatient(dateISO);
  return {
    year: date.year(),
    month: date.month(),
    week: date.isoWeek(),
    day: date.date()
  };
}

function aggregateByDay(patient, dateISO, cb) {
  var period = getPeriod(dateISO);
  UsageSessionModel.aggregate([{
    $match: {
      'patient.id': patient._id,
      date_begin: {
        $gte: period.begin,
        $lt: period.end
      }
    }
  }, {
    $group: {
      _id: null,
      "total_minutes": {
        "$sum": "$session_data.total_minutes"
      },
      "threat_minutes": {
        "$sum": "$session_data.threat_minutes"
      },
      "dusk_minutes": {
        "$sum": "$session_data.dusk_minutes"
      },
      "humpbacked_minutes": {
        "$sum": "$session_data.humpbacked_minutes"
      }
    }
  }], function(err, aggrData) { 
  	if (err) {
  		cb(err);
  	}
    delete aggrData[0]._id;
    AggregateDataModel.update({
      'patient.id': patient._id,
      interval_type: constants.INTERVAL_TYPE_DAY,
      interval_date: getDateObj(dateISO)
    }, {
      $set: {
        interval_data: aggrData[0],
      }
    }, {
      upsert: true
    }, function(err) {
      if (!err) {
        cb(null, patient, dateISO);
      } else {
      	cb(err);
      }
    });
  });
}

function aggregateByWeek(patient, dateISO, cb) {
  var dateObj = getDateObj(dateISO);
  AggregateDataModel.aggregate([{
    $match: {
      'patient.id': patient._id,
      interval_type: constants.INTERVAL_TYPE_DAY,
      'interval_date.year': dateObj.year,
      'interval_date.week': dateObj.week
    }
  }, {
    $group: {
      _id: null,
      "total_minutes": {
        "$sum": "$interval_data.total_minutes"
      },
      "threat_minutes": {
        "$sum": "$interval_data.threat_minutes"
      },
      "dusk_minutes": {
        "$sum": "$interval_data.dusk_minutes"
      },
      "humpbacked_minutes": {
        "$sum": "$interval_data.humpbacked_minutes"
      }
    }
  }], function(err, aggrData) {
  	if (err) {
  		cb(err);
  	}
    delete aggrData[0]._id;
    AggregateDataModel.update({
      'patient.id': patient._id,
      interval_type: constants.INTERVAL_TYPE_WEEK,
      'interval_date.year': dateObj.year,
      'interval_date.month':getDatePatient(dateISO).startOf('week').month(),
      'interval_date.week': dateObj.week,
      'interval_date.day': getDatePatient(dateISO).startOf('week').date()
    }, {
      $set: {
        interval_data: aggrData[0],
      }
    }, {
      upsert: true
    }, function(err) {
      if (!err) {
        cb(null, patient, dateISO);
      } else {
      	cb(err);
      }
    });
  });
}

function aggregateByMonth(patient, dateISO, cb) {
  var dateObj = getDateObj(dateISO);

  AggregateDataModel.aggregate([{
    $match: {
      'patient.id': patient._id,
      interval_type: constants.INTERVAL_TYPE_DAY,
      'interval_date.year': dateObj.year,
      'interval_date.month': dateObj.month
    }
  }, {
    $group: {
      _id: null,
      "total_minutes": {
        "$sum": "$interval_data.total_minutes"
      },
      "threat_minutes": {
        "$sum": "$interval_data.threat_minutes"
      },
      "dusk_minutes": {
        "$sum": "$interval_data.dusk_minutes"
      },
      "humpbacked_minutes": {
        "$sum": "$interval_data.humpbacked_minutes"
      }
    }
  }], function(err, aggrData) {
  	if (err) {
  		cb(err);
  	}
    delete aggrData[0]._id;
    AggregateDataModel.update({
      'patient.id': patient._id,
      interval_type: constants.INTERVAL_TYPE_MONTH,
      'interval_date.year': dateObj.year,
      'interval_date.month':dateObj.month,
      'interval_date.week': 1,
      'interval_date.day': 1
    }, {
      $set: {
        interval_data: aggrData[0],
      }
    }, {
      upsert: true
    }, function(err) {
      if (!err) {
        cb();
      } else {
      	cb(err);
      }
    });
  });
}

module.exports = function(req) { 
	var dateBegin = req.body.date_begin,
	patient = req.patient;
  async.waterfall([
    async.apply(aggregateByDay, req.patient, dateBegin),
    aggregateByWeek,
    aggregateByMonth
  ], function(err) {
    if (!err) {
      (new EventModel()).addLog(req, false, 'Данные успешно агрегированны');
    } else {
    	(new EventModel()).addLog(req, true, 'Не удалось агрегировать данные:' + err);
    }
  });
};