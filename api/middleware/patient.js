"use strict";
var PatientModel = require('../models/patient');
var url = require('url');
var qs = require('querystring');
var constants = require('../libs/constants');

exports.findByPromoCode = function(req, res, next) {
  var query = url.parse(req.url).query,
    parsed = qs.parse(query),
    promoCode = parsed.promo_code,
    error;

  if (!promoCode) {
    error = new Error('не указан промо код');
    error.status = 400;
    return next(error);
  }

  if (promoCode.length !== constants.PROMOCOD_LENGTH) {
    error = new Error('некорректный промо код');
    error.status = 403;
    return next(error);
  }

  PatientModel.findOne({
    promo_code: promoCode,
    status: {
      $in: [
        constants.PATIENT_STATUS_NOTACTIVATED,
        constants.PATIENT_STATUS_ACTIVE
      ]
    }
  }, function(err, patient) {
    if (err) {
      return next(err);
    }

    if (!patient) {
      error = new Error('промо код недействительный');
      error.status = 403;
      return next(error);
    }
    req.patient = patient;
    return next();
  });
};

exports.find = function(req, res, next) {
  var patientId = req.params.patient_id;
  var error;
  if (!patientId) {
    error = new Error('Id пациента не указан');
    error.status = 400;
    return next(error);
  }
  PatientModel.findById(patientId, function(err, patient) {
    if (err) {
      return next(err);
    }

    if (!patient) {
      error = new Error('Пациента с заданным id найти не удалось');
      error.status = 404;
      return next(error);
    }

    if (patient.status === constants.PATIENT_STATUS_STOPPED) {
      error = new Error('Пациент заблокирован');
      error.status = 403;
      return next(error);
    }

    if (patient.status === constants.PATIENT_STATUS_REMOVED) {
      error = new Error('Пациент удален');
      error.status = 404;
      return next(error);
    }

    req.patient = patient;
    return next();
  });
};
