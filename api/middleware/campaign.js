'use strict';
var CampaignModel = require('../models/campaign');
exports.find = function(req, res, next) {
	var campaignId = req.body.campaign.id;
	CampaignModel.findById(campaignId, function(err, campaign){
		if (err) {
			return next(err);
		}
		if (!campaign) {
			var error = new Error('Акция с заданным id не найдена');
			error.status = 404;
			return next(error);
		}
		req.campaign = campaign;
		return next();
	});
};