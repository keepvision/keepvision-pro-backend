var express = require('express');
var bodyParser = require('body-parser');
var methodOverride = require('method-override');
var path = require('path');
var EventModel = require('./models/event_log');
var config = require('./config/config');
require('moment').locale('ru');

var app = express();

app.use(methodOverride('X-HTTP-Method-Override'));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

var route = require('./routes/routes')(app);


app.use(function(err, req, res, next){ 
	(new EventModel()).addLog(req, true, err.message);
  return res.status(err.status || 500).send({ error: err.message });
});

app.listen(config.get('port'), function(){
  console.log('Express server listening on port ' + config.get('port'));
});

