var nconf = require('nconf');


var configFile = process.argv[2];

if (!configFile) {
	console.log('Не задан файл config');
}

nconf.argv()
  .env()
  .file({ file: configFile });

module.exports = nconf;