"use strict";
var mongoose = require('mongoose'),
  config = require('./config/config'),
  DELAY = config.get('delay'),
  MessageModel = require('./models/message'),
  constants = require('./constants'),
  db = mongoose.connection,
  gcm = require('node-gcm');


function processMessages(messages) {
  messages.forEach(function(message) {
    if (!message.patient.GCM_token) {
      return;
    }

    var pushMessage = new gcm.Message();
    var regTokens = [message.patient.GCM_token],
      sender = new gcm.Sender('AIzaSyDzcW06gWQ4e0x5K_zwtGAlTfThy0xF8a0');

    pushMessage.addNotification({
      title: message.content.title,
      body:  message.content.text,
      icon: 'ic_launcher'
    });

    sender.send(pushMessage, {
      registrationTokens: regTokens
    }, function(err, response) {
      if (err) {
        console.error('Ошибка при отправке push уведомления:' + err);
      } else {
        // console.log('success:' + message._id);
        message.status = constants.MESSAGE_STATUS_READ;
        message.save(function(err) {
          if (err) {
            console.error('Ошибка при смене статуса сообщения:' + err);
          }
        });
      }
    });
  });
}

function getMessage() {
  var yesterday = new Date();
  yesterday.setDate(yesterday.getDate() - 1);
  MessageModel.find({
    status: constants.MESSAGE_STATUS_NOTREAD,
    date_sent: {
      $gte: yesterday
    }
  }, function(err, messages) {
    if (err) {
      return console.error('Ошибка при получении сообщений:' + err);
    }
    processMessages(messages);
  });
}


function mainLoop() {
  function calculateDelay() {
    var delay = DELAY - (new Date() - startTime);
    return (delay < 0) ? 0 : delay;
  }
  var startTime = new Date();
  getMessage();
  setTimeout(mainLoop, calculateDelay());
}

mongoose.connect(config.get('mongoose:uri'));

db.on('error', function(err) {
  console.error('Ошибка при подключении к БД:' + err);
});
db.once('open', function callback() {
  console.log('Сервис обработки сообщений запущен');
  mainLoop();
});
