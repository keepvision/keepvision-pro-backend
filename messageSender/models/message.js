'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var messageSchema = new Schema({
  date_sent: Date,
  patient: {
    id: Schema.Types.ObjectId,
    first_name: String,
    last_name: String,
    mid_name: String,
    GCM_token: String
  },
  sender: {
    picture_url: String,
    name: String
  },
  content: {
    title: String,
    text: String,
    link:String,
    text2:String,
    link2:String,
    date:Date,
    type:Number,
    imageUrl: String,
    call_phone: String
  },
  status: Number
});


module.exports = mongoose.model('Message', messageSchema, 'message');
