Meteor.methods({
  insertEmployee: function(doc) {
    if (!utilServer.isAdmin(Meteor.userId())) throw new Meteor.Error('Необходимо авторизоваться');

    var userId = Accounts.createUser({
      email: doc.email,
      profile: doc.profile
    });
    var clinic = Collection.Clinic.findOne(new Mongo.ObjectID(doc.clinic_id));

    Meteor.users.update(userId, {
      $set: {
        status: doc.status,
        clinic:{
          id:clinic._id,
          name:clinic.name
        },
        date_update: new Date()
      }
    });

    switch (doc.role) {
      case CONSTANTS.EMPLOYEE_ROLE_MANAGER:
        Roles.setUserRoles(userId, ['manager']);
        break;
      case CONSTANTS.EMPLOYEE_ROLE_DOCTOR:
        Roles.setUserRoles(userId, ['doctor']);
        break;
      default:
        throw new Meteor.Error('Ошибка при добавлении роли');
    }

    Accounts.sendEnrollmentEmail(userId);
  },
  updateEmployee: function(mod, Id) {
    if (!utilServer.isAdmin(Meteor.userId())) throw new Meteor.Error('Необходимо авторизоваться');

    mod['$set'].date_update = new Date();
    Meteor.users.update(Id, mod);
  },
  deleteEmployee: function(employeeId) {
    if (!utilServer.isAdmin(Meteor.userId())) throw new Meteor.Error('Необходимо авторизоваться');
    Meteor.users.update(employeeId, {
      $set: {
        status: CONSTANTS.EMPLOYEE_STATUS_DELETED,
        date_update: new Date()
      }
    });
  }
});
