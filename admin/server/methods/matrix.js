Meteor.methods({
  saveMatrix: function(from, to, yellow, red) {
        if (!Meteor.userId()) {
            throw new Meteor.Error('non-authorized');
        }

        return Interval.insert({
            age_from: from,
            age_to: to,
            interval_yellow: yellow,
            interval_red: red
        });

    },

    editMatrix: function(id, from, to, yellow, red) {
        if (!Meteor.userId()) {
            throw new Meteor.Error('non-authorized');
        }

        return Interval.update(id, {
            $set: {
                age_from: from,
                age_to: to,
                interval_yellow: yellow,
                interval_red: red
            }
        });
    },

    deleteMatrix: function(id) {
        if (!Meteor.userId()) {
            throw new Meteor.Error('non-authorized');
        }

        return Interval.remove(id);
    },

    editMatrixFactor: function(id, age_from, age_to, value_from, value_to, factor) {
        if (!Meteor.userId()) {
            throw new Meteor.Error('non-authorized');
        }

        return Factor.update(id, {
            $set: {
                age_from: age_from,
                age_to: age_to,
                value_from: value_from,
                value_to: value_to,
                factor: factor
            }
        });
    },

    saveMatrixFactor: function(age_from, age_to, value_from, value_to, factor, code, name) {
        if (!Meteor.userId()) {
            throw new Meteor.Error('non-authorized');
        }

        return Factor.insert({
            name: name,
            code: code,
            age_from: age_from,
            age_to: age_to,
            value_from: value_from,
            value_to: value_to,
            factor: factor
        });

    },

    deleteMatrixFactor: function(id) {
            if (!Meteor.userId()) {
                throw new Meteor.Error('non-authorized');
            }

            return Factor.remove(id);
        } //,

    });