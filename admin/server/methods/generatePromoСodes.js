Meteor.methods({
  generatePromoCodes: function(amount) {
    var LENGTH_API_KEY = 20;
    var i = 0;
    var promoCode;
    while (i < amount) {
      promoCode = utilServer.generatePromoCode();
      if (!Collection.Patient.findOne({
          'promo_code': promoCode
        })) {
        Collection.Patient.insert({
          promo_code: promoCode,
          status: CONSTANTS.PATIENT_STATUS_NOTACTIVATED,
          api_key:Random.id([LENGTH_API_KEY]),
          date_update: new Date()
        });
        i++;
      }
    }
  }
});
