Meteor.methods({
  bindDoctor: function(doctorId, arrayPatientId) {
    var doctor = Meteor.users.findOne(doctorId),
      objectIdArray = [];

    arrayPatientId.forEach(function(id) {
      objectIdArray.push(new Mongo.ObjectID(id));
    });

    Collection.Patient.update({
      _id: {
        $in: objectIdArray
      }
    }, {
      $set: {
        doctor: {
          id: doctor._id,
          last_name: doctor.profile.last_name,
          first_name: doctor.profile.first_name,
          mid_name: doctor.profile.mid_name
        },
        clinic: doctor.clinic,
        date_update: new Date()
      }
    }, {
      multi: true
    }, function(err) {
      if (!err) {
        updateDoctorStat(doctorId);
      }
    });
  }
});


function updateDoctorStat(doctorId) {
  Collection.Patient.aggregate([{
      $match: {
        'doctor.id': doctorId
      }
    }, {
      $group: {
        "_id": '$status',
        "count": {
          "$sum": 1
        }
      }
    }],
    function(err, results) {
      let promoCodeStat,
        patientStat,
        countPromoCode,
        countPatient;

      promoCodeStat = _.find(results, function(result) {
        return result._id === CONSTANTS.PATIENT_STATUS_NOTACTIVATED;
      });

      patientStat = _.find(results, function(result) {
        return result._id === CONSTANTS.PATIENT_STATUS_ACTIVE;
      });

      countPromoCode = promoCodeStat ? promoCodeStat.count : 0;
      countPatient = patientStat ? patientStat.count : 0;


      Meteor.users.update(doctorId, {
        $set: {
          stats: {
            count_promo_code: countPromoCode,
            count_patient: countPatient
          }
        }
      });
    });
}
