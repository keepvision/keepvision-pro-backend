Meteor.methods({
  insertAdmin:function(doc) {
    if (!utilServer.isAdmin(Meteor.userId())) throw new Meteor.Error('Необходимо авторизоваться');
    var userId = Accounts.createUser({
      email:doc.email,
      profile: doc.profile
    });
    Meteor.users.update(userId, { $set:{
      status:doc.status,
      date_update: new Date()
    }});
    Roles.setUserRoles(userId, ['admin']);
    Accounts.sendEnrollmentEmail(userId);
  },
  editAdmin:function(mod, adminId) {
    if (!utilServer.isAdmin(Meteor.userId())) throw new Meteor.Error('Необходимо авторизоваться');
    mod['$set'].date_update = new Date();
    Meteor.users.update(adminId, mod);
  },
  deleteAdmin:function(idAdmin) {
    if (!utilServer.isAdmin(Meteor.userId())) throw new Meteor.Error('Необходимо авторизоваться');
    Meteor.users.update(idAdmin, { $set: {
      status:CONSTANTS.ADMIN_STATUS_DELETED, 
      date_update: new Date()
    }});
  }
});