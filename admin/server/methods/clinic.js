Meteor.methods({
  insertClinic:function(doc) {
    if (!utilServer.isAdmin(Meteor.userId())) throw new Meteor.Error('Необходимо авторизоваться');
    Collection.Clinic.insert(doc);
  },
  editClinic:function(mod, clinicId) {
    if (!utilServer.isAdmin(Meteor.userId())) throw new Meteor.Error('Необходимо авторизоваться');
    Collection.Clinic.update(clinicId, mod);
  },
  deleteClinic:function(clinicId) {
    if (!utilServer.isAdmin(Meteor.userId())) throw new Meteor.Error('Необходимо авторизоваться');
    Collection.Clinic.update(clinicId, { $set: {
      status:CONSTANTS.CLINIC_STATUS_REMOVED 
    }});
  }
});