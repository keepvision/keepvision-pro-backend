//Создаем первого админа
if (!Meteor.users.findOne({
    'roles': 'admin'
  })) {
  var id = Accounts.createUser({
    profile: {
      name: 'admin'
    },
    email: 'admin@admin',
    password: 'adminadmin'
  });
  Meteor.users.update(id, {
    $set: {
      status: CONSTANTS.ADMIN_STATUS_ACTIVE,
      date_update: new Date()
    }
  });
  Roles.setUserRoles(id, ['admin']);
}

