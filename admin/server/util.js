utilServer = {};
utilServer.arrayCharactersAllowed = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".split('');

utilServer.isAdmin = function(user) {
  return Roles.userIsInRole(user, 'admin');
};
utilServer.generatePromoCode = function() {
  var LENGTH_PROMO_CODE = 8,
  promoCode = '';
  for (var i = 0; i < LENGTH_PROMO_CODE; i++) {
  	promoCode += Random.choice(utilServer.arrayCharactersAllowed);
  }
  return promoCode;
};
