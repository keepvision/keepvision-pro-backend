Meteor.publish('employeeList', function(filter, clinicId, role) {
  if (!utilServer.isAdmin(this.userId)) {
    return [];
  }
 
  var conditions = {
    status: {
      $ne: CONSTANTS.EMPLOYEE_STATUS_DELETED
    },
    'clinic.id': clinicId,
    $or: [{
      roles: 'manager'
    }, {
      roles: 'doctor'
    }]
  };

  if (role) {
    conditions['roles'] = role;
  }

  if (filter) {
    conditions['profile.first_name'] = {
      $regex: filter + ".*",
      $options: 'i'
    };
  }

  return Meteor.users.find(conditions, {
    sort: {
      'profile.first_name': 1
    }
  });
});

Meteor.publish('oneEmployee', function(employeeId) {
  if (!utilServer.isAdmin(this.userId)) {
    return [];
  }
  return Meteor.users.find({
    _id: employeeId
  });
});
