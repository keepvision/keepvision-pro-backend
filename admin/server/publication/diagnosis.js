Meteor.publish('diagnosisPatient', function(patientId) {
  if (!utilServer.isAdmin(this.userId)) {
    return [];
  }
  return Collection.Diagnosis.find({
    'patient.id': patientId
  }, {
    sort: {
      date_update: 1
    }
  });
});
