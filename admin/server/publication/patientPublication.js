Meteor.publish('patientList', function(limit, fieldSearch) {
  if (!utilServer.isAdmin(this.userId)) {
    return [];
  }
  var conditions = {
    doctor: {
      $exists: true
    }
  };

  if (fieldSearch) {
    conditions['$or'] = [{
      'first_name': {
        $regex: fieldSearch + ".*",
        $options: 'i'
      }
    }, {
      'last_name': {
        $regex: fieldSearch + ".*",
        $options: 'i'
      }
    }, {
      'mid_name': {
        $regex: fieldSearch + ".*",
        $options: 'i'
      }
    }]
  }

  return Collection.Patient.find(conditions, {
    sort: {
      date_update: -1
    },
    limit: limit
  });
});

Meteor.publish('onePatient', function(patientId) {
  if (!utilServer.isAdmin(this.userId)) {
    return [];
  }

  return Collection.Patient.find({
    _id: patientId
  });
});
