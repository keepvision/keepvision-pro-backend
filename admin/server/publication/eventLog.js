Meteor.publish('eventLog', function(eventLogCode, dateFrom, dateTo, isError, limit) {
  if (!utilServer.isAdmin(this.userId)) {
    return []
  };

  var selectCondition = {};

  if (eventLogCode) {
    selectCondition.event_code = {
      $regex: eventLogCode + ".*",
      $options: 'i'
    };
  }
  if (typeof isError === 'boolean') selectCondition.is_error = isError;

  if (dateFrom) {
    selectCondition.event_date = selectCondition.event_date || {};
    selectCondition.event_date.$gte = dateFrom;
  }
  if (dateTo) {
    selectCondition.event_date = selectCondition.event_date || {};
    selectCondition.event_date.$lte = dateTo;
  }

  return Collection.eventLog.find(selectCondition, {
    limit: limit,
    sort: {
      event_date: -1
    }
  });
});
