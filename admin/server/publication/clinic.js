var LIMIT_DEFAULT = 10;
Meteor.publish('clinicList', function(filter, status) {
  if (!utilServer.isAdmin(this.userId)) {
    return [];
  }

  var conditions = {};

  if (status !== null && status !== undefined) {
    conditions.status = status;
  } else {
    conditions.status = {
      $ne: CONSTANTS.CLINIC_STATUS_REMOVED
    }
  }

  if (filter) {
    conditions.name = {
      $regex: filter + ".*",
      $options: 'i'
    };
  }

  return Collection.Clinic.find(conditions);
});

Meteor.publish('oneClinic', function(clinicId) {
  if (!utilServer.isAdmin(this.userId)) {
    return [];
  }
  return Collection.Clinic.find({
    _id: new Mongo.ObjectID(clinicId)
  });
});
