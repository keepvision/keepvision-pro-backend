Meteor.publish('promoCodeList', function(limit) {
  if (!utilServer.isAdmin(this.userId)) {
    return [];
  }
  
  return Collection.Patient.find({
    doctor:{$exists:false}
  }, {
    sort: {
      date_update: -1
    },
    limit: limit
  });
});
