Meteor.publish('admin', function() {
  if (!utilServer.isAdmin(this.userId)) return [];
  return Meteor.users.find({
    roles: 'admin',
    status: {
      $ne: CONSTANTS.ADMIN_STATUS_DELETED
    }
  }, {
    sort: {
      'profile.name': 1
    }
  });
});

Meteor.publish('oneAdmin', function(adminId) {
  if (!utilServer.isAdmin(this.userId)) return [];
  return Meteor.users.find({
    _id: adminId
  });
});
