Accounts.emailTemplates.siteName =  Meteor.settings.email.siteName;
Accounts.emailTemplates.from = Meteor.settings.email.from;

Accounts.emailTemplates.resetPassword.subject = function (user) {
  return "Восстановление пароля " + user.profile.name;
};
Accounts.emailTemplates.resetPassword.text = function (user, url) {
  return "Для восстановления пароля перейдите по ссылке ниже:\n\n" + url;
};

Accounts.emailTemplates.enrollAccount.subject = function (user) {
    return "Создана учетная запись";// + user.profile.name;
};
Accounts.emailTemplates.enrollAccount.text = function (user, url) {
	console.log(user);
	if (user.roles[0]==='admin'){
		return " Для активации перейдите по ссылке ниже:\n\n" + url;
	}
	 var address = url.split('#');
   return " Для активации перейдите по ссылке ниже:\n\n"
     +Meteor.settings.addressDoctorSys + '#' + address[1];
};

