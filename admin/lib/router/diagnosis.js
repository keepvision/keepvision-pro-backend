Router.route('/diagnosis/patientId/:patientId', {
  name: 'diagnosis',
  data: function() {
    return {
      patientId: new Mongo.ObjectID(this.params.patientId),
      patientIdStr:this.params.patientId
    };
  }
});
