Router.route('/csv/patientId/:patientId', {
  where: 'server',
  name: 'csv',
  action: function() {
    var patientId = this.params.patientId,
      content = '',
      patient = Collection.Patient.findOne(new Mongo.ObjectID(patientId)),
      fio = utilLib.getFullFIO(patient),
      filename = encodeURIComponent(fio) + '.csv',
      headers = {
        'Content-type': 'text/csv',
        'Content-Disposition': "attachment; filename*=UTF-8''" + filename
      };


    var diagnosisData = Collection.Diagnosis.find({
      'patient.id': new Mongo.ObjectID(patientId)
    }, {
      sort: {
        date_update: -1
      }
    }).fetch();

    content = 'ФИО, пол, возраст, дата, оба, левый, правый, освещенность, начальная дистанция \r\n';

    diagnosisData.forEach(function(diagnos) {
      var data = diagnos.diagnosis_data;
      content += fio + ',' + CONSTANTS.PATIENT_GENDER_NAME[patient.gender] + ',' + moment().diff(patient.birthdate, 'years') + ',' +
        moment(diagnos.date_update).format("DD.MM.YYYY hh:mm:ss") + ',' + data.testResult.both + ',' + data.testResult.left + ',' +
        data.testResult.right + ',' + data.lux + ',' + data.startDistance + '\r\n';
    });

    this.response.writeHead(200, headers);
    return this.response.end(content);
  }
});
