Router.route('/employeeAdd/clinicId/:clinicId', {
  name: 'employeeAdd',
  data: function() {
    return this.params.clinicId;
  }
});

Router.route('/employeeEdit/employeeId/:employeeId', {
  name: 'employeeEdit',
  waitOn: function() {
    return Meteor.subscribe('oneEmployee', this.params.employeeId);
  },
  data: function() {
    return Meteor.users.findOne(this.params.employeeId);
  }
});
