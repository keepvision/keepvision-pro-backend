Router.route('/clinicAdd', {
  name: 'clinicAdd'
});

Router.route('/clinicList', {
  name: 'clinicList'
});

Router.route('/clinicEdit/clinicId/:clinicId', {
  name: 'clinicEdit',
  waitOn: function() {
    return Meteor.subscribe('oneClinic', this.params.clinicId);
  },
  data: function() {
    return Collection.Clinic.findOne(new Mongo.ObjectID(this.params.clinicId));
  }
});
