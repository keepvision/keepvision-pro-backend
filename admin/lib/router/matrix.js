Router.route('intervalMatrix', {
	path: '/settings/interval_matrix',
	waitOn: function() {
		return [
			Meteor.subscribe('matrix')
		]
	},
	data: function() {
		Session.set('addMatrix', false);
		
		return {
			matrix: Interval.find().fetch()
		};
	}
});

Router.route('factorMatrix', {
	path: '/settings/factor_matrix',
	waitOn: function() {
		Session.set('durationBreaks', true);
		Session.set('addMatrix', false);
		Session.set('editMatrix', false);

		return [
			Meteor.subscribe('matrixByCode')
		]
	}
});