Router.route('/adminAdd', {
	name:'adminAdd'
});

Router.route('/adminList', {
	name:'adminList'
});

Router.route('/adminEdit/adminId/:adminId', {
	name:'adminEdit',
	waitOn:function() {
		return Meteor.subscribe('oneAdmin', this.params.adminId);
	},
	data:function() {
		return Meteor.users.findOne(this.params.adminId);
	}
});