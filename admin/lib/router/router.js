Router.configure({
  layoutTemplate: 'layout',
  loadingTemplate: 'loading'
});


Router.map(function() {
  this.route('authorizationForm', {
    path: '/'
  });

  this.route('forgotPassword', {
    path: '/forgotPassword'
  });
});

var requireLogin = function() {
  if (!Meteor.user()) {
    if (Meteor.loggingIn()) {
      this.render(this.loadingTemplate);
    } else {
      Router.go('/');
    }
  }
  this.next();
}

var notShowAuthorization = function() {
  if (Meteor.user()) {
    Router.go('clinicList');
  }
  this.next();
}

var routeAuth = ['authorizationForm', 'forgotPassword'];

Router.onBeforeAction('loading');
Router.onBeforeAction(requireLogin, {
  except: ['authorizationForm', 'forgotPassword', 'csv']
});
Router.onBeforeAction(notShowAuthorization, {
  only: routeAuth
});
