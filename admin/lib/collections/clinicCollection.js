this.Collection = this.Collection || {};
this.Schems = this.Schems || {};

Collection.Clinic = new Mongo.Collection('clinic', {
  idGeneration: 'MONGO'
});

Schems.Clinic = new SimpleSchema({
  name: {
    type: String,
    label: "Название"
  },
  paid_up: {
    type: Date,
    label: "Оплачено до",
    autoform: {
      type: "bootstrap-datepicker",
      datePickerOptions: {
        language: "ru-RU",
        format: "dd.mm.yyyy"
      }
    }
  },
  status: {
    type: Number,
    allowedValues: [
      CONSTANTS.CLINIC_STATUS_NEW,
      CONSTANTS.CLINIC_STATUS_ACTIVE,
      CONSTANTS.CLINIC_STATUS_STOPPED,
      CONSTANTS.CLINIC_STATUS_REMOVED,
    ],
    label: "Статус",
    autoform: {
      type: "select",
      options: function() {
        return [{
          label: CONSTANTS.CLINIC_STATUS_NAME[CONSTANTS.CLINIC_STATUS_NEW],
          value: CONSTANTS.CLINIC_STATUS_NEW
        }, {
          label: CONSTANTS.CLINIC_STATUS_NAME[CONSTANTS.CLINIC_STATUS_ACTIVE],
          value: CONSTANTS.CLINIC_STATUS_ACTIVE
        }, {
          label: CONSTANTS.CLINIC_STATUS_NAME[CONSTANTS.CLINIC_STATUS_STOPPED],
          value: CONSTANTS.CLINIC_STATUS_STOPPED
        }];
      }
    }
  },
  phone: {
    type: String,
    optional:true
  }
});

Collection.Clinic.attachSchema(Schems.Clinic);
