this.Collection = this.Collection || {};
this.Schems = this.Schems || {};

Collection.Patient = new Mongo.Collection('patient', {
  idGeneration: 'MONGO'
});

Schems.Patient = new SimpleSchema({
  promo_code: {
    type: String,
    label: "Промо код",
    optional:true
  },
  status: {
    type: Number,
    label: "Статус",
    optional:true
  },
  doctor:{
  	type:Object,
  	blackbox:true,
  	optional:true

  },
  clinic:{
  	type:Object,
  	blackbox:true,
  	optional:true

  },
  last_name:{
    type:String,
    label: 'Фамилия',
    optional:true
  },
  first_name:{
    type:String,
    label: 'Имя',
    optional:true

  },
  mid_name:{
    type:String,
    label:'Отчество',
    optional:true

  },
  birthdate: {
    type: Date,
    label: "День рождения",
    autoform: {
      type: "bootstrap-datepicker",
      datePickerOptions: {
        language: "ru-RU",
        format:"dd.mm.yyyy"
      }
    },
    optional:true
  },
  gender:{
  	type:Number,
  	label:"Пол",
  	optional:true

  },
  api_key:{
  	type:String,
  	optional:true
  },
  date_update:{
    type:Date,
    optional:true
  }
});

Collection.Patient.attachSchema(Schems.Patient);
