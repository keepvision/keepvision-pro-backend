this.Schems = this.Schems || {};
Schems.authorization = new SimpleSchema({
	email:{
		type:String,
		regEx: SimpleSchema.RegEx.Email,
    label:'Email'
	},
	password:{
		type:String,
		label:'Пароль',
		min:6
	}
});

Schems.forgotPassword = new SimpleSchema({
	email:{
		type:String,
		regEx: SimpleSchema.RegEx.Email,
    label:'email'
	}
});