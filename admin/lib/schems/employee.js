this.Schems = this.Schems || {};

var profileSchema = new SimpleSchema({
  mid_name:{
    type:String,
    label:'Отчество',
    optional:true
  },
  first_name:{
    type:String,
    label: 'Имя'
  },
  last_name:{
    type:String,
    label: 'Фамилия'
  }
});

Schems.employee = new SimpleSchema({
  profile:{
    type:profileSchema
  },
  email: {
    type: String,
    label: 'E-mail',
    regEx: SimpleSchema.RegEx.Email
  },
  status: {
    type: Number,
    allowedValues: [
      CONSTANTS.EMPLOYEE_STATUS_ACTIVE,
      CONSTANTS.EMPLOYEE_STATUS_BLOCKED,
      CONSTANTS.EMPLOYEE_STATUS_DELETED
    ],
    label: "Статус",
    autoform: {
      type: "select",
      options: function() {
        return [{
          label: CONSTANTS.EMPLOYEE_STATUS_NAME[CONSTANTS.EMPLOYEE_STATUS_ACTIVE],
          value: CONSTANTS.EMPLOYEE_STATUS_ACTIVE
        }, {
          label: CONSTANTS.EMPLOYEE_STATUS_NAME[CONSTANTS.EMPLOYEE_STATUS_BLOCKED],
          value: CONSTANTS.EMPLOYEE_STATUS_BLOCKED
        }];
      }
    }
  },
  role:{
    type:Number,
    label:'Роль',
    autoform:{
      type:"select",
       options: function() {
        return [{
          label: CONSTANTS.EMPLOYEE_ROLE_NAME[CONSTANTS.EMPLOYEE_ROLE_MANAGER],
          value: CONSTANTS.EMPLOYEE_ROLE_MANAGER
        }, {
          label: CONSTANTS.EMPLOYEE_ROLE_NAME[CONSTANTS.EMPLOYEE_ROLE_DOCTOR],
          value: CONSTANTS.EMPLOYEE_ROLE_DOCTOR
        }];
      }
    }
  },
  clinic_id:{
    type:String
  }
});



Schems.employeeEdit = Schems.employee.pick(['profile.mid_name', 'profile.first_name', 'profile.last_name', 'status']);