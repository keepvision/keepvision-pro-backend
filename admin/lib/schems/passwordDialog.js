this.Schems = this.Schems || {};
Schems.changePassword = new SimpleSchema({
	passwordOld: {
		label:"Старый пароль",
		type:String,
		min:6
	},
	password: {
		label:"Пароль",
		type:String,
		min:6
	},
	passwordCopy: {
		label:"Повторите пароль",
		type:String,
		min:6,
		custom: function () {
      if (this.value !== this.field('password').value) {
        return "passwordMismatch";
      }
    }
	}
});

Schems.recoveryPassword = new SimpleSchema({
	password: {
		label:"Пароль",
		type:String,
		min:6
	},
	passwordCopy: {
		label:"Повторите пароль",
		type:String,
		min:6,
		custom: function () {
      if (this.value !== this.field('password').value) {
        return "passwordMismatch";
      }
    }
	}
});