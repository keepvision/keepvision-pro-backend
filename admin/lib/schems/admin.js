this.Schems = this.Schems || {};
Schems.admin = new SimpleSchema({
  'profile.name': {
    type: String,
    label: 'Имя'
  },
  email: {
    type: String,
    label: 'email',
    regEx: SimpleSchema.RegEx.Email
  },
  status: {
    type: Number,
    allowedValues: [
      CONSTANTS.ADMIN_STATUS_ACTIVE,
      CONSTANTS.ADMIN_STATUS_BLOCKED,
      CONSTANTS.ADMIN_STATUS_DELETED
    ],
    label: "Статус",
    // optional: true,
    autoform: {
      type: "select",
      options: function() {
        return [{
          label: "Действует",
          value: CONSTANTS.ADMIN_STATUS_ACTIVE
        }, {
          label: "Заблокирован",
          value: CONSTANTS.ADMIN_STATUS_BLOCKED
        }];
      }
    }
  }
});


Schems.adminEdit = Schems.admin.pick(['profile.name', 'status']);