this.Schems = this.Schems || {};
Schems.intervalMatrix = new SimpleSchema({
	age_from:{
		type: Number,
		min: 0,
		max: 100,
		custom: function () {
			if (this.value > this.field('age_to').value) {
				return "ageValue";
			}
		}
	},
	age_to:{
		type: Number,
		min: 0,
		max: 100,
		custom: function () {
			if (this.value < this.field('age_from').value) {
				return "ageValue";
			}
		}	
	},
	yellow:{
		type: Number,
		min: 0,
		max: 40,
		custom: function () {
			if (this.value > this.field('red').value) {
				return "redYellow";
			}
		}
	},
	red:{
		type: Number,
		min: 0,
		max: 60,
		custom: function () {
			if (this.value < this.field('yellow').value) {
				return "redYellow";
			}
		}	
	}
});

Schems.factorMatrix = new SimpleSchema({
	age_from:{
		type: Number,
		min: 0,
		max: 100,
		custom: function () {
			if (this.value > this.field('age_to').value) {
				return "ageValue";
			}
		}
	},
	age_to:{
		type: Number,
		min:0,
		max: 100,
		custom: function () {
			if (this.value < this.field('age_from').value) {
				return "ageValue";
			}
		}
	},
	value_from:{
		type: Number,
		min: 0,
		max: 1000000,
		decimal: true,
		custom: function () {
			if (this.value > this.field('value_to').value) {
				return "value";
			}
		}
	},
	value_to:{
		type: Number,
		min: 0,
		max: 1000000,
		decimal: true,
		custom: function () {
			if (this.value < this.field('value_from').value) {
				return "value";
			}
		}
	},
	factor:{
		type: Number,
		min: 0,
		max: 100
	}
});