utilLib = {
    getFullFIO(man) {
      let fio = '';
      if (man.last_name) {
        fio += man.last_name + ' ';
      }
      if (man.first_name) {
        fio += man.first_name + ' ';
      }
      if (man.mid_name) {
        fio += man.mid_name;
      }
      return fio;
    }
};
