Accounts.onEnrollmentLink( function(token, done) {
	Session.set('tokenReserPassword', token);
	$(document).ready(function() {
		Modal.show('passwordRecoveryDialog');
	});
});

Accounts.onResetPasswordLink( function(token, done) {
	Session.set('tokenReserPassword', token);
	$(document).ready(function() {
		Modal.show('passwordRecoveryDialog');
	});
});