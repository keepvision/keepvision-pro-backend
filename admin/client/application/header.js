Template.header.helpers({
	userName:function() {
		var user = Meteor.user();
		return user && user.emails[0].address;
	}
});

Template.header.events( {
	'click #exit':function() {
		Meteor.logout();
		Router.go('/');
	},
	'click #changePass':function() {
		Modal.show('passwordChangeDialog');
	}
});

