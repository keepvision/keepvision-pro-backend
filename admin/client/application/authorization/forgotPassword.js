var message = new ReactiveVar();
Template.forgotPassword.events({
	'submit form':function(e) {
		e.preventDefault();
		var email = $('[name=email]').val();
		Accounts.forgotPassword({email:email}, function(err) {
			if (err) message.set(err.reason);
			else message.set('На '+ email + ' отправлено письмо с инструкциями для восстановления пароля.');
		});
	}
});

Template.forgotPassword.helpers({
	message:function() {
		return message.get();
	}
});