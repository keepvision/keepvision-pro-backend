var message = new ReactiveVar();
Template.authorizationForm.events({
	'submit form':function(e) {
		e.preventDefault();
		message.set('');
		var email = $('[name=email]').val(),
		password = $('[name=password]').val();
		// remebMe = $("#remember-me-pass").prop('checked');

		// if (remebMe) $.cookie('email', email);
		// else $.removeCookie('email');

		Meteor.loginWithPassword(email, password, function(err) {
        if (!err) {
           Router.go('clinicList');
        } else {
        	message.set(err.reason);
        }
    });
	}
});

Template.authorizationForm.helpers({
	message:function() {
		return message.get();
	}
});