Template.promoCodeItem.helpers({
  date_update: function() {
    return moment(this.date_update).format("DD.MM.YYYY HH:mm:ss");
  },
  promoCodeId: function() {
    return this._id.valueOf();
  }
});

Template.promoCodeItem.events({
  'change .cbPromoCode': function(e) {
    var checkbox = $(e.currentTarget);
    var lineTable = checkbox.parent().parent();

    if (checkbox.is(":checked")) {
      lineTable.removeClass('hide-print');
    } else {
      lineTable.addClass('hide-print');
    }
  }
});
