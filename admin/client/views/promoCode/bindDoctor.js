var clinicId = new ReactiveVar();
Template.bindDoctor.onCreated(function(){
	var self = this;
	self.subscribe('clinicList');
	self.autorun(function() {
		self.subscribe('employeeList', null, clinicId.get(), 'doctor'); 
	});
});


Template.bindDoctor.helpers({
	clinicList:function() {
		return Collection.Clinic.find();
	},
  doctorList:function() {
  	return Meteor.users.find({'roles':'doctor'});
  }
});

Template.bindDoctor.events({
	'click tr.clinicLine':function() {
		clinicId.set(this._id);
		$('#clinic-list').hide(200);
		$('#doctor-list').show(200);
	},
	'click tr.doctorLine':function() {
		var doctorId = this._id;
		var checkedIdArray = $("input.cbPromoCode:checked").parent().parent().map(function(){
		    return $(this).data('id');
		}).get();
		Meteor.call('bindDoctor', doctorId, checkedIdArray, function() {
			Modal.hide();
		});
	}
});