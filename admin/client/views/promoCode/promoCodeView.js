var LIMIT_LINE_DEFAULT = 30;
var limitLine = new ReactiveVar();

Template.promoCodeView.onCreated(function() {
  var self = this;
  limitLine.set(LIMIT_LINE_DEFAULT);
  self.autorun(function() {
    self.subscribe('promoCodeList', limitLine.get());
  });
  $(window).scroll(function() {
    if ($(window).scrollTop() == $(document).height() - $(window).height()) {
      limitLine.set(limitLine.get() + LIMIT_LINE_DEFAULT);
    }
  });
});

Template.promoCodeView.helpers({
  promoCodeList: function() {
    return Collection.Patient.find({}, {
      sort: {
        date_update: -1
      }
    });
  }
});

Template.promoCodeView.events({
  'click #generatePromoCodeBtn': function() {
  	var amount = +$('#amountPromoCode').val();
  	if (isNaN(amount)) {
  		return;
  	}
  	Meteor.call('generatePromoCodes', amount)
  },
  'click #bideDoctorBtn':function() {
    Modal.show('bindDoctor');
  },
  'click #printBtn':function() {
    window.print();
  }
});
