var LIMIT_LINE_DEFAULT = 30;
var limitLine = new ReactiveVar(),
  fioField = new ReactiveVar();

Template.patientList.onCreated(function() {
  var self = this;
  limitLine.set(LIMIT_LINE_DEFAULT);
  fioField.set(null);
  self.autorun(function() {
    self.subscribe('patientList', limitLine.get(), fioField.get());
  });
  $(window).scroll(function() {
    if ($(window).scrollTop() == $(document).height() - $(window).height()) {
      limitLine.set(limitLine.get() + LIMIT_LINE_DEFAULT);
    }
  });
});

Template.patientList.helpers({
  patientList: function() {
    return Collection.Patient.find({}, {
      sort: {
        date_update: -1
      }
    });
  },
  fio: function() {
    if (this.last_name || this.first_name || this.mid_name) {
      return this.last_name + ' ' + this.first_name + ' ' + this.mid_name;
    }
  },
  status: function() {
    return CONSTANTS.PATIENT_STATUS_NAME[this.status];
  },
  date_update: function() {
    return moment(this.date_update).format("DD.MM.YYYY hh:mm:ss");
  },
  birthdateFormat: function() {
    return moment(this.birthdate).format("DD.MM.YYYY");
  },
  genderFormat: function() {
    return CONSTANTS.PATIENT_GENDER_NAME[this.gender];
  }
});

Template.patientList.events({
  'keyup .searchByName': function(e) {
    fioField.set(e.currentTarget.value);
  },
  'click .diagnosisList': function(e) {
    Router.go('diagnosis', {
      patientId: this._id.valueOf()
    })
  }
});
