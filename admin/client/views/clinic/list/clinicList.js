var fieldSearch = new ReactiveVar(),
  status = new ReactiveVar(),
  sortField = new ReactiveVar();

Template.clinicList.onCreated(function() {
  var self = this;
  fieldSearch.set(null);
  status.set(null);
  sortField.set('paid_up')
  self.autorun(function() {
    self.subscribe('clinicList', fieldSearch.get(), status.get());
  });
});

Template.clinicList.helpers({
  clinicList: function() {
    if (sortField.get() === 'name') {
      var sort = {
        'name': 1
      };
    } else {
      sort = {
        'paid_up': 1
      };
    }
    return Collection.Clinic.find({}, {
      sort: sort
    });
  }
});

Template.clinicList.events({
  'click #add-button': function() {
    Router.go('clinicAdd');
  },
  'keyup .searchByName': function(e) {
    fieldSearch.set(e.currentTarget.value);
  },
  'change #searchByStatus': function(e) {
    if (e.currentTarget.value === 'all') {
      status.set(null);
    } else {
      status.set(+e.currentTarget.value)
    }
  },
  'click #sortByPaidUp': function() {
    sortField.set('paidUp');
  },
  'click #sortByName': function() {
    sortField.set('name');
  }
});
