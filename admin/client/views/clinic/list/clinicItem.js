Template.clinicItem.helpers({
	statusText:function() {
		return CONSTANTS.CLINIC_STATUS_NAME[this.status];
	},
	paid_up:function(){
		return moment(this.paid_up).format("DD.MM.YYYY");
	}
});

Template.clinicItem.events({
	'click tr':function() {
		Router.go('clinicEdit',{clinicId:this._id.valueOf()});
	}
});