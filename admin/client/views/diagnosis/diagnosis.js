Template.diagnosis.onRendered(function() {
  this.subscribe('diagnosisPatient', this.data.patientId);
  this.subscribe('onePatient', this.data.patientId);
});

Template.diagnosis.helpers({
  diagnosisList: function() {
    return Collection.Diagnosis.find({}, {
      sort: {
        date_update: -1
      }
    });
  },
  date_update:function() {
  	 return moment(this.date_update).format("DD.MM.YYYY hh:mm:ss");
  }, 
  fio:function() {
  	var patient = Collection.Patient.findOne();
  	return patient && utilLib.getFullFIO(patient);
  },
  gender: function() {
  	var patient = Collection.Patient.findOne();
    return patient && CONSTANTS.PATIENT_GENDER_NAME[patient.gender];
  },
  age: function() {
  	var patient = Collection.Patient.findOne();
  	return patient && moment().diff(patient.birthdate, 'years');
  }
});
