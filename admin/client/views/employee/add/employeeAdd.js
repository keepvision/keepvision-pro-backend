Template.employeeAdd.events({
  'click .btn-cancel': function() {
    history.back();
  }
});

var hooksObject = {
  before:{
    'method':function(doc) {
      doc.clinic_id = this.template.data.clinicId;
      return doc;
    }
  },
  after: {
    'method': function(error, result) {
      if (error) {
        return alert(error.reason);
      }
      history.back();
    }
  }
};

AutoForm.addHooks('insertEmployeeForm', hooksObject);
