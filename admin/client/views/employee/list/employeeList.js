var fieldSearch = new ReactiveVar();

Template.employeeList.onCreated(function() {
  var self = this,
    clinic = Collection.Clinic.findOne();
  self.autorun(function() {
    self.subscribe('employeeList', fieldSearch.get(), clinic._id);
  });
});

Template.employeeList.helpers({
  employeeList: function() {
    return Meteor.users.find({
      $or: [{
        roles: 'manager'
      }, {
        roles: 'doctor'
      }]
    }, {
      sort: {
        'profile.first_name': 1
      }
    });
  }
});

Template.employeeList.events({
  'click #add-button-employee': function() {
    Router.go('employeeAdd', {
      clinicId: this._id.valueOf()
    });
  }
});
