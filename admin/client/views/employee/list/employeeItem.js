Template.employeeItem.helpers({
  statusText: function() {
    return CONSTANTS.EMPLOYEE_STATUS_NAME[this.status];
  },
  roleText: function() {
    if (this.roles[0] === 'doctor') {
      return CONSTANTS.EMPLOYEE_ROLE_NAME[CONSTANTS.EMPLOYEE_ROLE_DOCTOR];
    }
    if (this.roles[0] === 'manager') {
      return CONSTANTS.EMPLOYEE_ROLE_NAME[CONSTANTS.EMPLOYEE_ROLE_MANAGER];
    }
  }
});

Template.employeeItem.events({
  'click tr': function() {
    Router.go('employeeEdit',{employeeId:this._id.valueOf()});
  }
});
