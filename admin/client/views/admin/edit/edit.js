Template.adminEdit.events({
  'click .btn-cancel': function() {
    history.back();
  },
  'click .btn-remove': function() {
    if (confirm("Удалить?")) {
      Meteor.call('deleteAdmin', this._id, function(err){
      	if (err) {
      		return alert(err.reason);
      	}
      	history.back();   		
      });
    }
  }
});



var hooksObject = {
  after: {
   'method-update': function(error, result) {
   		if (error) {
   			return alert(error.reason);
   		}
   		history.back();
   }
  }
};

AutoForm.addHooks('updateAdminForm', hooksObject);