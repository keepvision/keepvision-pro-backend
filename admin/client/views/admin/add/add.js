Template.adminAdd.events({
	'click .btn-cancel':function() {
		history.back();
	}
});

var hooksObject = {
  after: {
   'method': function(error, result) {
   		if (error) {
   			return alert(error.reason);
   		}
   		history.back();
   }
  }
};

AutoForm.addHooks('insertAdminForm', hooksObject);