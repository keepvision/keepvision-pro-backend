Template.adminList.onCreated(function(){
	this.subscribe('admin');
});

Template.adminList.helpers({
	adminList:function() {
		return Meteor.users.find({},{sort:{'profile.name':1}});
	}
});

Template.adminList.events({
	'click #add-button':function() {
		Router.go('adminAdd');
	}
});