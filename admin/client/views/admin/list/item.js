Template.adminItem.helpers({
	statusText:function() {
		return CONSTANTS.ADMIN_STATUS_NAME[this.status];
	}
});

Template.adminItem.events({
	'click tr':function() {
		Router.go('adminEdit',{adminId:this._id});
	}
});