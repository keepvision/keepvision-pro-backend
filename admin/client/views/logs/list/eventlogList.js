var LIMIT_LINE = 50;
var code = new ReactiveVar(),
dateFrom = new ReactiveVar(),
dateTo = new ReactiveVar(),
status = new ReactiveVar(),
limitLine = new ReactiveVar();

Template.eventLogList.onCreated(function () {
	limitLine.set(LIMIT_LINE);
	var self = this;
	self.autorun(function () {
  	self.subscribe('eventLog', code.get(), dateFrom.get(), dateTo.get(), status.get(), limitLine.get());
	});
	 $(window).scroll(function() {
    if ($(window).scrollTop() == $(document).height() - $(window).height()) {
      limitLine.set(limitLine.get() + LIMIT_LINE);
    }
  });
});

Template.eventLogList.onRendered(function() {
	 $('#dateFrom').datetimepicker({locale:'ru'});
	 $('#dateTo').datetimepicker({locale:'ru'});
});

Template.eventLogList.onDestroyed(function() {
	code.set(null);
	status.set(null);
	dateFrom.set(null);
	dateTo.set(null);
});

Template.eventLogList.helpers({
	eventLogList:function(){
		return Collection.eventLog.find({},{sort:{event_date:-1}}).fetch();
	}
});

Template.eventLogList.events({
	'keyup #code':function(e) {
		code.set(e.currentTarget.value);
	},
	'dp.change #dateFrom':function(e) {
		if (e.date) dateFrom.set(new Date(e.date));
	},
	'dp.change #dateTo':function(e) {
		if (e.date) dateTo.set(new Date(e.date));
	},
	'change #status':function(e) {
		switch(e.currentTarget.value) {
			case 'true':
				status.set(true)
			break;
			case 'false':
				status.set(false)
			break;
			case 'null':
				status.set(null);
			break;
		}
	}
});
