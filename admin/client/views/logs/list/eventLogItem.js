Template.eventLogItem.helpers({
	event_date:function(){
		return moment(this.event_date).format('D.M.YYYY HH:mm:ss');
	}
});

Template.eventLogItem.events({
	'click tr':function(){
		Modal.show('viewLog',{logId:this._id.valueOf()});
	}
});