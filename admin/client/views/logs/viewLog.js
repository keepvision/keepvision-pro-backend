// Template.viewLog.created = function(){
// 	var logId = this.data.logId,
// 	self = this;

// 	self.autorun(function () {
//   	self.subscribe('eventLogOne', logId);
// 	 });
// };

Template.viewLog.helpers({
	log:function(){
		var eventLog = Collection.eventLog.findOne(new Mongo.ObjectID(this.logId));
		if (!eventLog) return;
		var par = [];
		for(var key in eventLog) {
			if (key === 'value') {
				for (var keyValue in eventLog.value) {
						par.push({'text':keyValue + ':' + eventLog.value[keyValue]});
				}
			}
			else 	par.push({'text':key + ':' + eventLog[key]});
		}
		return par;
	}
});