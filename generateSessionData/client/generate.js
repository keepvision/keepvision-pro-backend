Template.generateSession.events({
  'click button' () {
    let patientId = $('#patientId').val(); 
    patient = Collection.Patient.findOne(new Mongo.ObjectID(patientId));
 		if (!patient) return;

 		let countSession = +$('#countSession').val(),
 		dateBegin = new Date($('#dateBegin').val()),
 		dateEnd = new Date($('#dateEnd').val());

    Meteor.call('generateSession', patient, countSession, dateBegin, dateEnd, function(err, result){
    	console.log(err);
    });
  }
});
