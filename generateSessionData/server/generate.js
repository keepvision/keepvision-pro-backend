Meteor.methods({
  generateSession: function(patient, countSession, dateBegin, dateEnd) {
    let crypto = Npm.require('crypto');
    for (var i = 0; i < countSession; i++) {
      let dateRandom = new Date(getRandomInt(+dateBegin, +dateEnd)),
        dateBeginISO = moment(dateRandom).format(),
        patientId = patient._id.valueOf(),
        totalMinutes = getRandomInt(1, 100),
        threatMinutes = getRandomInt(0, totalMinutes),
        duskMinutes = getRandomInt(0, totalMinutes),
        humpbackedMinutes = getRandomInt(0, totalMinutes),
        strForHash = patientId + dateBeginISO + totalMinutes + threatMinutes + duskMinutes +
        humpbackedMinutes + patient.api_key,
        hash = crypto.createHash('sha256').update(strForHash).digest('hex');
      
      var path = `${Meteor.settings.serviceApiIp}:${Meteor.settings.serviceApiPort}` +
        `/api/v2/patients/${patientId}/usage_sessions?security_token=${hash}`;

      try {
        var result = HTTP.call("POST", path, {
          data: {
            "date_begin": dateBeginISO,
            "session_data": {
              "total_minutes": totalMinutes,
              "threat_minutes": threatMinutes,
              "dusk_minutes": duskMinutes,
              "humpbacked_minutes": humpbackedMinutes
            }
          }
        });
      } catch (err) {
      	console.log(err);
        throw new Meteor.Error(err);
      }
    };
  }
});

function getRandomInt(min, max) {
  return Math.floor(Math.random() * (max - min + 1)) + min;
}
