Router.route('/doctor', {
  name: 'doctor'
});

Router.route('/doctor/doctor_id/:doctor_id', {
  name: 'editDoctor',
  waitOn: function() {
    return Meteor.subscribe('oneDoctor', this.params.doctor_id);
  },
  data: function() {
    return Meteor.users.findOne(this.params.doctor_id);
  }
});
