Router.configure({
  layoutTemplate: 'layout',
  loadingTemplate: 'loading'
});

var requireLogin = function() {
  if (!Meteor.user()) {
    if (Meteor.loggingIn()) {
      this.render(this.loadingTemplate);
    } else {
      Router.go('landingPage');
    }
  }
  $('body').removeClass('landing-page');
  $('body').addClass('basic-page');
  this.next();
}

var notShowAuthorization = function() {
  let user = Meteor.user(),
    template;
  if (user) {
    if ('manager' === user.roles[0]) {
      template = 'manager';
    } else if ('doctor' === user.roles[0]) {
      template = 'doctor';
    }
    Router.go(template);
  }
  $('body').removeClass('basic-page');
  $('body').addClass('landing-page');
  this.next();
}

var routeAuth = ['landingPage', 'login', 'addClinic'];

Router.onBeforeAction('loading');

Router.onBeforeAction(requireLogin, {
  except: routeAuth
});

Router.onBeforeAction(notShowAuthorization, {
  only: routeAuth
});
