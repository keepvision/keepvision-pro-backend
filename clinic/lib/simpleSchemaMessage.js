SimpleSchema.messages({
	required:'Поле "[label]" необходимо заполнить',
	minNumber:'[label] должен быть минимум [min]',
	minString:'[label] должен быть минимум [min] символов',
	regEx: [
		{exp: SimpleSchema.RegEx.Email, msg: "должен быть правильным e-mail адресом"}
	],
	'passwordMismatch':'пароли не совпадают'
});