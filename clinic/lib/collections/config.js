this.Collection = this.Collection || {};

Collection.Config = new Mongo.Collection('config', {
  idGeneration: 'MONGO'
});