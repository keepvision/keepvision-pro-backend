this.Collection = this.Collection || {};

Collection.AggregatedData = new Mongo.Collection('aggregated_data', {
  idGeneration: 'MONGO'
});