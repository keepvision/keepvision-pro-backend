this.Collection = this.Collection || {};
this.Schems = this.Schems || {};

Collection.Campaign = new Mongo.Collection('campaign', {
  idGeneration: 'MONGO'
});

var interfaceSchema = new SimpleSchema({
  all_patient: {
    type: Boolean,
    label:'Все пациенты'
  },
  man: {
    type: Boolean,
    label:'Мужчины'
  },
  woman: {
    type: Boolean,
    label:'Женщины'
  },
  to_17_years: {
    type: Boolean,
    label:'до 17 лет'
  },
  from_18_to_24_years: {
    type: Boolean,
    label:'от 18 до 24 лет'
  },
  from_24_to_40_years: {
    type: Boolean,
    label:'от 24 до 40 лет'
  },
  from_41_and_older: {
    type: Boolean,
    label:'41 год и старше'
  }
});

Schems.Campaign = new SimpleSchema({
  date_begin: {
    type: Date,
    label: 'Дата начала',
    optional: true
  },
  clinic: {
    type: Object,
    blackbox: true,
    optional: true
  },
  status: {
    type: Number,
    optional: true
  },
  content: {
    type: Object,
    blackbox: true,
    optional: true
  },
  interface: {
    type: interfaceSchema
  },
  stats:{
    type:Object,
    blackbox:true,
    optional:true
  },
  conditionSelect:{
    type:String,
    optional:true
  }
});

Collection.Campaign.attachSchema(Schems.Campaign);

Schems.addCampaign = new SimpleSchema({
  title: {
    type: String,
    label: 'Заголовок'
  },
  text: {
    type: String,
    label: 'Текст акции'
  },
  date_begin: {
    type: Date,
    label: "Дата рассылки",
    optional:true
  },
  interface: {
    type: interfaceSchema
  }
});
