this.Collection = this.Collection || {};
this.Schems = this.Schems || {};

Collection.Message = new Mongo.Collection('message', {
  idGeneration: 'MONGO'
});

var senderScheme = new SimpleSchema({
  picture_url: {
    type: String
  },
  name: {
    type: String
  }
});

var contentScheme = new SimpleSchema({
  title: {
    type: String
  },
  text: {
    type: String
  },
  imageUrl: {
    type: String
  },
  call_phone: {
    type: String
  },
  date: {
    type:Date
  },
  type:{
    type:Number
  }
});

Schems.messageSchema = new SimpleSchema({
  date_sent: {
    type: Date
  },
  patient: {
    type:Object,
    blackbox: true
  },
  sender: {
    type: senderScheme
  },
  content: {
    type: contentScheme
  },
  status: {
    type:Number
  }
});
