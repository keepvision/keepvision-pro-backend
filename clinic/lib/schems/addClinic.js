this.Schems = this.Schems || {};

var profileSchema = new SimpleSchema({
  mid_name:{
    type:String,
    label:'Отчество',
    optional:true
  },
  first_name:{
    type:String,
    label: 'Имя'
  },
  last_name:{
    type:String,
    label: 'Фамилия'
  }
});

Schems.addClinic = new SimpleSchema({
  profile:{
    type:profileSchema
  },
  email: {
    type: String,
    label: 'Эл. почта',
    regEx: SimpleSchema.RegEx.Email
  },
  nameClinic:{
  	type:String,
  	label:'Название клиники'
  },
  phone:{
  	type:String,
  	label:'Телефон'
  }
});
