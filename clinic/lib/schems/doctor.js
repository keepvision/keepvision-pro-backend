this.Schems = this.Schems || {};

var profileSchema = new SimpleSchema({
  mid_name:{
    type:String,
    label:'Отчество',
    optional:true
  },
  first_name:{
    type:String,
    label: 'Имя'
  },
  last_name:{
    type:String,
    label: 'Фамилия'
  }
});

Schems.doctor = new SimpleSchema({
  profile:{
    type:profileSchema
  },
  email: {
    type: String,
    label: 'Эл. почта',
    regEx: SimpleSchema.RegEx.Email
  }
});



Schems.editDoctor = Schems.doctor.pick(['profile.mid_name', 'profile.first_name', 'profile.last_name']);