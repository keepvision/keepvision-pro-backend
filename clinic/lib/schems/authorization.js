this.Schems = this.Schems || {};
Schems.authorization = new SimpleSchema({
	email:{
		type:String,
		regEx: SimpleSchema.RegEx.Email,
    label:'Эл.почта'
	},
	password:{
		type:String,
		label:'Пароль',
		min:6
	}
});

Schems.forgotPassword = new SimpleSchema({
	email:{
		type:String,
		regEx: SimpleSchema.RegEx.Email,
    label:'E-mail'
	}
});