Meteor.publish('oneClinic', function() {
  if (!(utilServer.isManager(this.userId) || utilServer.isDoctor(this.userId))) {
    return [];
  }
  let clinicId = Meteor.users.findOne(this.userId).clinic.id;

  return Collection.Clinic.find({
    _id: clinicId
  });
});
