Meteor.publish("userData", function() {
  if (this.userId) {
    return Meteor.users.find({
      _id: this.userId
    }, {
      fields: {
        'clinic': 1,
        'stats': 1
      }
    });
  } else {
    this.ready();
  }
});
