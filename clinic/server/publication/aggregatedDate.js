Meteor.publish('AggregatedDataWeeks', function(patientId, weeks) {
  if (!utilServer.isDoctor(this.userId)) {
    return [];
  }

  return Collection.AggregatedData.find({
    interval_type: CONSTANTS.INTERVAL_TYPE_DAY,
    $or: [{
      'interval_date.year': weeks[0].year,
      'interval_date.week': weeks[0].number
    }, {
      'interval_date.year': weeks[1].year,
      'interval_date.week': weeks[1].number
    }],
    'patient.id': patientId
  }, {
    sort: {
      'interval_date.day': 1
    }
  });

});

Meteor.publish('AggregatedDataMonth', function(patientId, month) {
  if (!utilServer.isDoctor(this.userId)) {
    return [];
  }

  return Collection.AggregatedData.find({
    interval_type: CONSTANTS.INTERVAL_TYPE_WEEK,
    $or: [{
      'interval_date.month': month[0].number,
      'interval_date.year': month[0].year
    }, {
      'interval_date.month': month[1].number,
      'interval_date.year': month[1].year
    }, {
      'interval_date.month': month[2].number,
      'interval_date.year': month[2].year
    }],
    'patient.id': patientId
  }, {
    sort: {
      'interval_date.week': 1
    }
  });
});

// Meteor.publish('AggregatedDataYear', function(patientId, year) {
//   if (!utilServer.isDoctor(this.userId)) {
//     return [];
//   }

//   return Collection.AggregatedData.find({
//     interval_type: CONSTANTS.INTERVAL_TYPE_MONTH,
//     'interval_date.year': year,
//     'patient.id': patientId
//   }, {
//     sort: {
//       'interval_date.month': 1
//     }
//   });
// });

// Meteor.publish('AggregatedData', function(patientId) {
//   if (!utilServer.isDoctor(this.userId)) {
//     return [];
//   }

//   return Collection.AggregatedData.find({
//     'patient.id': patientId
//   });
// });
