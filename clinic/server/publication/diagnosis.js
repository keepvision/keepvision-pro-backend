Meteor.publish('DiagnosisWeeks', function(patientId, dateSelect) {
  if (!utilServer.isDoctor(this.userId)) {
    return [];
  }
  let beginInterval = moment(dateSelect).subtract(1,'week').startOf('week').toDate(),
  endInterval = moment(dateSelect).endOf('week').toDate();


  return Collection.Diagnosis.find({
    date_update:{
      '$gte':beginInterval,
      '$lte':endInterval
    },
    'patient.id': patientId
  }, {
    sort: {
      date_update: 1
    }
  });
});

Meteor.publish('DiagnosisMonth', function(patientId, dateSelect) {
  if (!utilServer.isDoctor(this.userId)) {
    return [];
  }
  let beginInterval = moment(dateSelect).subtract(2,'month').startOf('month').toDate(),
  endInterval = moment(dateSelect).endOf('month').toDate();

  return Collection.Diagnosis.find({
    date_update:{
      '$gte':beginInterval,
      '$lte':endInterval
    },
    'patient.id': patientId
  }, {
    sort: {
      date_update: 1
    }
  });
});