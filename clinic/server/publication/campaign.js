Meteor.publish('campaignList', function() {
  if (!utilServer.isManager(this.userId)) {
    return [];
  }
  let clinicId = Meteor.users.findOne(this.userId).clinic.id;
  return Collection.Campaign.find({
   'clinic.id' : clinicId,
   status:CONSTANTS.CAMPAIGN_STATUS_ACTIVE 
  });
});
