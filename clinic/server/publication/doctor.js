Meteor.publish('doctorForClinic', function() {
  if (!utilServer.isManager(this.userId)) {
    return [];
  }
  let clinicId = Meteor.users.findOne(this.userId).clinic.id;
  return Meteor.users.find({
    'clinic.id': clinicId,
    status:CONSTANTS.EMPLOYEE_STATUS_ACTIVE,
    roles:'doctor'
  });
});

Meteor.publish('oneDoctor', function(doctorId) {
	let doctor = Meteor.users.findOne(doctorId);
  if (!utilServer.isManager(this.userId, doctor.clinic.id)) {
    return [];
  }
 
  return Meteor.users.find({
    '_id': doctorId,
    status:CONSTANTS.EMPLOYEE_STATUS_ACTIVE,
    roles:'doctor'
  });
});