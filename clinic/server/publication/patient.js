Meteor.publish('patientOneDoctor', function(fieldSearch) {
  if (!utilServer.isDoctor(this.userId)) {
    return [];
  }

  conditions = {
    'doctor.id': this.userId,
    status: CONSTANTS.PATIENT_STATUS_ACTIVE
  };
  if (fieldSearch) {
    conditions['$or'] = [{
      'first_name': {
        $regex: fieldSearch + ".*",
        $options: 'i'
      }
    }, {
      'last_name': {
        $regex: fieldSearch + ".*",
        $options: 'i'
      }
    }, {
      'mid_name': {
        $regex: fieldSearch + ".*",
        $options: 'i'
      }
    }]
  }

  return Collection.Patient.find(conditions, {
    fields: {
      last_name: 1,
      first_name: 1,
      mid_name: 1,
      birthdate: 1,
      gender: 1
    }
  });
});

// Meteor.publish('onePatient', function(patientId) {
//   if (!utilServer.isDoctor(this.userId)) {
//     return [];
//   }

//   return Collection.Patient.find({
//     _id: new Mongo.ObjectID(patientId)
//   }, {
//     fields: {
//       last_name: 1,
//       first_name: 1,
//       mid_name: 1,
//       birthdate: 1,
//       gender: 1
//     }
//   });
// });
