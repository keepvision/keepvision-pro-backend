Accounts.config({
	forbidClientAccountCreation:true
});

Accounts.validateLoginAttempt(function(par) {
	if (par.error) {
		throw new Meteor.Error(403, par.error.message);
	}
	if (!(Roles.userIsInRole(par.user, "doctor") || Roles.userIsInRole(par.user, "manager"))) {
		throw new Meteor.Error(403, "Нет прав");
	}
	if (par.user.status !== CONSTANTS.EMPLOYEE_STATUS_ACTIVE) {
		throw new Meteor.Error(403, "Учетная запись не активирована");
	}
	return true;
});