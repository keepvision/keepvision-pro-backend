utilServer = {};
utilServer.arrayCharactersAllowed = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".split('');

utilServer.isManager = function(userId) {
  if (!userId) {
    return false;
  }

  let user = Meteor.users.findOne(userId);

  return (Roles.userIsInRole(userId, 'manager') && user.status === CONSTANTS.EMPLOYEE_STATUS_ACTIVE);
};

utilServer.isDoctor = function(userId) {
  if (!userId) {
    return false;
  }

  let user = Meteor.users.findOne(userId);

  return (Roles.userIsInRole(userId, 'doctor') && user.status === CONSTANTS.EMPLOYEE_STATUS_ACTIVE);
};

utilServer.generatePromoCode = function() {
  var LENGTH_PROMO_CODE = 8,
    promoCode = '';
  for (var i = 0; i < LENGTH_PROMO_CODE; i++) {
    promoCode += Random.choice(utilServer.arrayCharactersAllowed);
  }
  return promoCode;
};

utilServer.getFullFIO = function(man) {
  let fio = '';
  if (man.last_name) {
    fio += man.last_name + ' ';
  }
  if (man.first_name) {
    fio += man.first_name + ' ';
  }
  if (man.mid_name) {
    fio += man.mid_name;
  }
  return fio;
}


utilServer.getSelectCondition = function(interface) {
  let condition = {};
  if (interface.all_patient) {
    return condition;
  }

  if (interface.man && interface.woman) {
    condition.gender = {
      $exists: true
    };
  } else if (interface.man) {
    condition.gender = CONSTANTS.PATIENT_GENDER_MALE;
  } else if (interface.woman) {
    condition.gender = CONSTANTS.PATIENT_GENDER_FEMALE;
  }

  if (!interface.to_17_years && !interface.from_18_to_24_years && !interface.from_24_to_40_years && !interface.from_41_and_older) {
    return condition;
  }

  if (interface.to_17_years && interface.from_18_to_24_years && interface.from_24_to_40_years && interface.from_41_and_older) {
    condition.birthdate = {
      $exists: true
    };
  }

  condition['$or'] = [];

  if (interface.to_17_years) {
    condition['$or'].push({
      birthdate: {
        $gte: moment().subtract(17, "year").toDate()
      }
    });
  }

  if (interface.from_18_to_24_years) {
    condition['$or'].push({
      birthdate: {
        $gte: moment().subtract(24, "year").toDate(),
        $lte: moment().subtract(18, "year").toDate()
      }
    });
  }

  if (interface.from_24_to_40_years) {
    condition['$or'].push({
      birthdate: {
        $gte: moment().subtract(40, "year").toDate(),
        $lte: moment().subtract(24, "year").toDate()
      }
    });
  }

  if (interface.from_41_and_older) {
    condition['$or'].push({
      birthdate: {
        $lte: moment().subtract(41, "year").toDate()
      }
    });
  }
  return condition;
}
