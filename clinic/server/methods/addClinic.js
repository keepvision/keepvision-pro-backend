Meteor.methods({
  addClinic(doc) {
    let clinic = {
      name: doc.nameClinic,
      status: CONSTANTS.CLINIC_STATUS_NEW,
      phone: doc.phone,
      paid_up:moment().add(1, 'month').toDate() //+1 месяц
    };

    var clinicId = Collection.Clinic.insert(clinic)

    var userId = Accounts.createUser({
      email: doc.email,
      profile: doc.profile
    });

    Meteor.users.update(userId, {
      $set: {
        status: CONSTANTS.EMPLOYEE_STATUS_ACTIVE,
        clinic: {
          id: clinicId,
          name: clinic.name
        },
        date_update: new Date()
      }
    });

    Roles.setUserRoles(userId, ['manager']);
    Accounts.sendEnrollmentEmail(userId);
  }
});
