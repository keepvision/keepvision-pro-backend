Meteor.methods({
  addDoctor(doc) {
      let manager = Meteor.user(),
        clinicId = manager.clinic.id;

      if (!utilServer.isManager(manager._id, clinicId)) {
        throw new Meteor.Error('Необходимо авторизоваться');
      }

      let doctorId = Accounts.createUser({
        email: doc.email,
        profile: doc.profile
      });

      // saveImage(doc.image, doctorId + '_doctor.jpg');

      let clinic = Collection.Clinic.findOne(clinicId);

      Meteor.users.update(doctorId, {
        $set: {
          status: CONSTANTS.EMPLOYEE_STATUS_ACTIVE,
          clinic: {
            id: clinic._id,
            name: clinic.name
          },
          date_update: new Date()
        }
      });

      Roles.setUserRoles(doctorId, ['doctor']);
      Accounts.sendEnrollmentEmail(doctorId);
      return doctorId;
    },
    editDoctor(mod, doctorId) {
      let manager = Meteor.user(),
        clinicId = manager.clinic.id;

      if (!utilServer.isManager(manager._id, clinicId)) {
        throw new Meteor.Error('Необходимо авторизоваться');
      }
      
      Meteor.users.update(doctorId, mod);
      return doctorId;
    }
});

