Meteor.methods({
  inviteReception(patientId) {
    if (!utilServer.isDoctor(Meteor.userId())) {
      throw new Meteor.Error('Необходимо авторизоваться');
    }
    let inviteText = Collection.Config.findOne({
        key: 'invite_msg_text'
      }),
      inviteTitle = Collection.Config.findOne({
        key: 'invite_msg_title'
      }),
      path = Npm.require('path');
    inviteText = inviteText ? inviteText.value : '';
    inviteTitle = inviteTitle ? inviteTitle.value : '';

    let doctor = Meteor.user(),
      patient = Collection.Patient.findOne(patientId),
      message = {
        date_sent: new Date(),
        patient: {
          id: patient._id,
          last_name: patient.last_name,
          first_name: patient.first_name,
          mid_name: patient.mid_name,
          GCM_token: patient.GCM_token
        },
        sender: {
          picture_url: path.join(Meteor.absoluteUrl(), '/img-doctor/', doctor._id + '_doctor.jpg'),
          name: utilServer.getFullFIO(doctor.profile)
        },
        content: {
          title: inviteTitle,
          text: inviteText,
          date: new Date(),
          type: CONSTANTS.MESSAGE_TYPE_INVITATION
        },
        status: CONSTANTS.MESSAGE_STATUS_NOTREAD
      };
    Collection.Message.insert(message);
  }
})
