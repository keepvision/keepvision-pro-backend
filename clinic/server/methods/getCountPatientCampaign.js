Meteor.methods({
	getCountPatientCampaign(interface){
		let manager = Meteor.user();
    if (!manager) {
      return;
    }
    let condition = utilServer.getSelectCondition(interface);
    condition['clinic.id'] = manager.clinic.id;
    return Collection.Patient.find(condition).count();
	}
})