Meteor.methods({
  getPromoCode() {
    let LENGTH_API_KEY = 20,
    doctor = Meteor.user(),
    promoCode;
    if (!doctor) {
      return;
    }

    while (true) {
      promoCode = utilServer.generatePromoCode();
      if (!Collection.Patient.findOne({
          'promo_code': promoCode
        })) {
        Collection.Patient.insert({
          promo_code: promoCode,
          doctor:{
            id:doctor._id,
            last_name:doctor.profile.last_name,
            first_name:doctor.profile.first_name,
            mid_name:doctor.profile.mid_name
          },
          status: CONSTANTS.PATIENT_STATUS_NOTACTIVATED,
          api_key:Random.id([LENGTH_API_KEY]),
          date_update: new Date()
        });
        break;
      }
    }

    updateDoctorStat(doctor._id);
    return promoCode;
  }
});

function updateDoctorStat(doctorId) {
  Collection.Patient.aggregate([{
      $match: {
        'doctor.id': doctorId
      }
    }, {
      $group: {
        "_id": '$status',
        "count": {
          "$sum": 1
        }
      }
    }],
    function(err, results) {
      let promoCodeStat,
        patientStat,
        countPromoCode,
        countPatient;

      promoCodeStat = _.find(results, function(result) {
        return result._id === CONSTANTS.PATIENT_STATUS_NOTACTIVATED;
      });

      patientStat = _.find(results, function(result) {
        return result._id === CONSTANTS.PATIENT_STATUS_ACTIVE;
      });

      countPromoCode = promoCodeStat ? promoCodeStat.count : 0;
      countPatient = patientStat ? patientStat.count : 0;


      Meteor.users.update(doctorId, {
        $set: {
          stats: {
            count_promo_code: countPromoCode,
            count_patient: countPatient
          }
        }
      });
    });
}
