Meteor.methods({
  saveImage(blob, fileName) {
    let fs = Npm.require('fs'),
      path = Npm.require('path');

    fs.writeFile(path.join(Meteor.settings.pathImagesServer, fileName), blob, 'base64', function(err) {
      if (err) {
      	console.log('Файл сохранить не удалось:' + err);
      } 
    });
  }
});
