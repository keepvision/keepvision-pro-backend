Meteor.methods({
  addCampaign(doc) {
      if (!utilServer.isManager(Meteor.userId())) {
        throw new Meteor.Error('Необходимо авторизоваться');
      }

      let user = Meteor.user(),
        campaign = {};

      campaign.date_begin = new Date();
      campaign.clinic = user.clinic;
      campaign.status = CONSTANTS.CAMPAIGN_STATUS_ACTIVE;
      campaign.content = {
        title: doc.title,
        text: doc.text
      }
      campaign.interface = doc.interface;
      let condition = utilServer.getSelectCondition(doc.interface);
      campaign.conditionSelect = JSON.stringify(condition),
        clinic = Collection.Clinic.findOne(user.clinic.id);
      condition['clinic.id'] = clinic._id;

      let patients = Collection.Patient.find(condition).fetch();

      campaign.stats = {
        count_patient: patients.length,
        count_action_read: 0,
        count_action_call: 0
      };

      let campaignId = Collection.Campaign.insert(campaign);

      patients.forEach(function(patient) {
        let message = {
          date_sent: new Date(),
          patient: {
            id: patient._id,
            last_name: patient.last_name,
            first_name: patient.first_name,
            mid_name: patient.mid_name,
            GCM_token: patient.GCM_token
          },
          sender: {
            // picture_url: 
            name: clinic.name
          },
          content: {
            title: doc.title,
            text: doc.text,
            imageUrl: Meteor.absoluteUrl() + 'img-campaign/' + campaignId + '_campaign.jpg',
            call_phone: clinic.phone,
            date: new Date(),
            type: CONSTANTS.MESSAGE_TYPE_CAMPAIGN
          },
          status: CONSTANTS.MESSAGE_STATUS_NOTREAD
        };
        Collection.Message.insert(message);
      });

      return campaignId;
    },
    removeCampaign(campaignId) {
      if (!utilServer.isManager(Meteor.userId())) {
        throw new Meteor.Error('Необходимо авторизоваться');
      }
      Collection.Campaign.update(campaignId, {
        $set: {
          status: CONSTANTS.CAMPAIGN_STATUS_REMOVED
        }
      })
    }
});
