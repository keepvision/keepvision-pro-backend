Meteor.subscribe("userData");
// Meteor.Spinner.options = { top:'150px' }
Meteor.startup(() => {
  Meteor.autorun(() => {
    let user = Meteor.user(),
      clinicId = user && user.clinic && user.clinic.id;
    if (!user) {
      return;
    }
    Meteor.subscribe('oneClinic');
    if (user.roles[0] === 'manager') {
      Meteor.subscribe('doctorForClinic');
    }
  });
});
