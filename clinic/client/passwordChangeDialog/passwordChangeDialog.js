Template.passwordChangeDialog.events({
	'submit form':function(e) {
		e.preventDefault();
		var password = $(e.currentTarget).find('[name = password]').val(),
				passwordOld = $(e.currentTarget).find('[name = passwordOld]').val();
		
		Accounts.changePassword(passwordOld, password, function(err){
			if (!err) Modal.hide('passwordChangeDialog');
			else alert(err);	
		});
	},
	'click .btn-close':function() {
		Modal.hide('passwordChangeDialog');
	}
});
