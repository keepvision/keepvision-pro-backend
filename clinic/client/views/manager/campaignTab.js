let imageCampaignFR = new FileReader(),
  dateBeginRV = new ReactiveVar(),
  countPatient = new ReactiveVar(),
  titlePhone = new ReactiveVar(),
  textPhone = new ReactiveVar(),
  currentDate = new ReactiveVar();

Template.campaignTab.onCreated(function() {
  this.subscribe('campaignList');
});

Template.campaignTab.onRendered(function() {
  this.$('.datetimepicker').datetimepicker();
  Meteor.setInterval(function() {
    currentDate.set(moment());
  }, 1000);

  Meteor.call('getCountPatientCampaign', {}, function(err, count) {
    if (!err) {
      countPatient.set(count);
    }
  });
});

Template.campaignTab.helpers({
  campaignList() {
      return Collection.Campaign.find({}, {
        sort: {
          date_begin: 1
        }
      });
    },
    date_begin() {
      return this.date_begin && moment(this.date_begin).format("DD.MM.YYYY");
    },
    campaign_id() {
      return this._id.valueOf();
    },
    count_patient() {
      return countPatient.get();
    },
    title_phone() {
      return titlePhone.get();
    },
    text_phone() {
      return textPhone.get();
    },
    current_date() {
      return currentDate.get() && currentDate.get().format("D MMMM H:mm");
    },
    random_numb() { // Для борьбы с кэширование
      return Math.random();
    }
});

Template.campaignTab.events({
  'change #input-campaign-photo' (e) {
    let file = event.srcElement.files[0];
    if (!file) return;
    imageCampaignFR.onload = function() {
      $('#img-phone').attr('src', imageCampaignFR.result);
    };
    imageCampaignFR.readAsDataURL(file);
  },
  'click .btn-close' (e) {
    history.back();
  },
  'click #removeCampaign' () {
    Meteor.call('removeCampaign', this._id);
  },
  'dp.change .datetimepicker': function(e) {
    if (e.date) dateBeginRV.set(new Date(e.date));
  },
  'click .checkbox-campaign' () {
    let interface = {};
    interface.all_patient = $('[name="interface.all_patient"]').is(":checked");
    interface.man = $('[name="interface.man"]').is(":checked");
    interface.woman = $('[name="interface.woman"]').is(":checked");
    interface.to_17_years = $('[name="interface.to_17_years"]').is(":checked");
    interface.from_18_to_24_years = $('[name="interface.from_18_to_24_years"]').is(":checked");
    interface.from_24_to_40_years = $('[name="interface.from_24_to_40_years"]').is(":checked");
    interface.from_41_and_older = $('[name="interface.from_41_and_older"]').is(":checked");

    Meteor.call('getCountPatientCampaign', interface, function(err, count) {
      if (!err) {
        countPatient.set(count);
      }
    });
  },
  'keyup #title' (e) {
    titlePhone.set(e.currentTarget.value);
  },
  'keyup #text' (e) {
    textPhone.set(e.currentTarget.value);
  }
});

var hooksObject = {
  before: {
    method: function(doc) {
      doc.date_begin = dateBeginRV.get();
      return doc;
    }
  },
  after: {
    method: function(err, campaignId) {
      if (err) {
        return alert(err);
      }
      if (imageCampaignFR.result) {
        let image = imageCampaignFR.result.replace(/^data:image\/(png|jpeg);base64,/, "");
        Meteor.call('saveImage', image, campaignId + '_campaign.jpg', function(err) {
          if (!err) {
            Meteor.setTimeout(function() {
              $('#photo_' + campaignId).attr('src', '/img-campaign/' + campaignId + '_campaign.jpg?' + Math.random());
            }, 200);
          }
        });
      }
    }
  }
};

AutoForm.addHooks('campaignForm', hooksObject);
