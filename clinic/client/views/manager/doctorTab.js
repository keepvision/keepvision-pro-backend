Template.doctorTab.helpers({
  doctorList() {
      return Meteor.users.find({
        roles: 'doctor'
      }, {
        sort: {
          'profile.last_name': 1
        }
      });
    },
    doctor_id() {
      return this._id.valueOf();
    }
});


Template.doctorTab.events({
  'click #add-doctor-btn' () {
    Modal.show('addDoctor');
  },
  'click .table-btn-edit' () {
    Router.go('editDoctor', {
      'doctor_id': this._id
    })
  }
})
