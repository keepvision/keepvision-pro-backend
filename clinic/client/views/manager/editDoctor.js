let sizeZone;
Template.editDoctor.onRendered(() => {
  let aspectRatio = 1 / 1;
  $('#imageForCrop').Jcrop({
    onChange: updatePreview,
    onSelect: updatePreview,
    setSelect: [0, 0, 999999, 999999],
    aspectRatio: aspectRatio,
    boxWidth: 400,
    boxHeight: 400
  });
});

Template.editDoctor.helpers({
  doctor_id() {
      return this._id.valueOf();
    },
    random_numb() { // Для борьбы с кэширование
      return Math.random();
    }
});

Template.editDoctor.events({
  'change #imageDoctorInput' (e) {
    let imageDoctorFR = new FileReader(),
      file = event.srcElement.files[0],
      $imageForCrop = $('#imageForCrop');
    if (!file) return;

    if ($imageForCrop.data('Jcrop')) {
      $imageForCrop.data('Jcrop').destroy();
      $('#imageForCrop').removeAttr('style');
    }
    imageDoctorFR.onload = function(file) {
      let aspectRatio = 1 / 1;
      $imageForCrop.attr('src', imageDoctorFR.result);
      jcropApi = $imageForCrop.Jcrop({
        onChange: updatePreview,
        onSelect: updatePreview,
        setSelect: [0, 0, 999999, 999999],
        aspectRatio: aspectRatio,
        boxWidth: 400,
        boxHeight: 400
      });
    }
    imageDoctorFR.readAsDataURL(file);
  },
  'click .btn-close' (e) {
    history.back();
  }
});

function updatePreview(c) {
  sizeZone = c;
};

var hooksObject = {
  after: {
    'method-update': function(err, doctorId) {
      if (err) {
        return alert(err);
      }
      if (parseInt(sizeZone.w) > 0) {
        let imageObj = $("#imageForCrop")[0];
        canvas = document.createElement("canvas");
        canvas.width = sizeZone.w;
        canvas.height = sizeZone.h;
        var ctx = canvas.getContext("2d");
        ctx.drawImage(imageObj, sizeZone.x, sizeZone.y, sizeZone.w, sizeZone.h, 0, 0, canvas.width, canvas.height);
        let image = canvas.toDataURL("image/jpeg", 0.5).replace(/^data:image\/(png|jpeg);base64,/, "");
        Meteor.call('saveImage', image, doctorId + '_doctor.jpg', function() {
         
          history.back();
          //обновить картинку в списке
          Meteor.setTimeout(function() {
            $('#photo_' + doctorId).attr('src', '/img-doctor/' + doctorId + '_doctor.jpg?' + Math.random());
          }, 200);
        });
      } else {
        history.back();
      }
    }
  }
};

AutoForm.addHooks('editDoctor', hooksObject);
