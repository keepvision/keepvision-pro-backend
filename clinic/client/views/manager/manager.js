Template.manager.helpers({
  fio_manager() {
    let profile = Meteor.user() && Meteor.user().profile;
    if (profile) {
      return `${profile.last_name} ${profile.first_name[0]}.${profile.mid_name[0]}.`;
    }
  }
});

Template.manager.events({
  'click .item' (e) {
    let $itemClick = $(e.currentTarget),
      $itemNotClick;

    if (!$itemClick.hasClass('active')) {
      $itemNotClick = $('.fast-nav > .item.active');
      $itemClick.addClass('active');
      $itemNotClick.removeClass('active');

      if ($itemClick.data('header') === 'campaign') {
        $('#tabCampaign').show('fast');
        $('#tabDoctor').hide('fast');
      } else if ($itemClick.data('header') === 'doctor') {
        $('#tabCampaign').hide('fast');
        $('#tabDoctor').show('fast');
      }
    }
  }
});
