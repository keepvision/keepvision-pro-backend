let text = new ReactiveVar();
Template.addClinic.onCreated(function(){
  text.set('');
});

Template.addClinic.helpers({
  text(){
    return text.get();
  }
});

var hooksObject = {
  after: {
    method: function(err) {
    	 if (err) {
    	 	return text.set(err);
    	 }
    	 text.set('Клиника успешно добавлена. На почту выслана инструкция по авторизации');
    }
  }
};

AutoForm.addHooks('addClinicForm', hooksObject);