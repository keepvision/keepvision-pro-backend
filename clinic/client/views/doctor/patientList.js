 Template.patientList.helpers({
   patient_list() {
       return Collection.Patient.find();
     },
     patientId() {
       return this._id.valueOf();
     },
     fio_patient() {
       return utilClient.getFullFIO(this);
     },
     birthdate_patient() {
       return this.birthdate && moment(this.birthdate).format("DD.MM.YYYY");
     }
 });

 Template.patientList.events({
   'click .invite-reception' () {
     Meteor.call('inviteReception', this._id, function(err) {
       if (err) {
         return;
       };
       Modal.show('messageDialog', {
         titleMessage: 'Приглашение на прием',
         textMessage: 'Приглашение на прием успешно отправлено'
       });
     });
   }
 });
