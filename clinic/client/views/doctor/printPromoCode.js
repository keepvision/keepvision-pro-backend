Template.printPromoCode.helpers({
  clinic_name() {
      var clinic = Collection.Clinic.findOne();
      return clinic && clinic.name;
    },
    clinic_address() {
      var clinic = Collection.Clinic.findOne();
      return clinic && clinic.address;
    },
    doctor_name() {
      return Meteor.user() && utilClient.getFullFIO(Meteor.user().profile);
    },
    promo_code() {
      return this;
    }
});
