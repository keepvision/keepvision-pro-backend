let promoCode = new ReactiveVar(),
  fieldSearch = new ReactiveVar(),
  currentTemplate;

Template.doctor.onCreated(function() {
  fieldSearch.set(null);
  this.autorun(() => {
    this.subscribe('patientOneDoctor', fieldSearch.get());
  });
});

Template.doctor.onRendered(function() {
  currentTemplate = Blaze.render(Template.patientList, document.getElementById('template'));
});

Template.doctor.helpers({
  fio_doctor() {
      let profile = Meteor.user() && Meteor.user().profile;
      if (profile) {
        return `${profile.last_name} ${profile.first_name[0]}.${profile.mid_name[0]}.`;
      }
    },
    doctor_id() {
      let user = Meteor.user();
      return user && user._id.valueOf();
    },
    all_patient_count() {
      let user = Meteor.user();
      return user && user.stats && user.stats.count_patient;
    },
    generatePromoCode() {
      return promoCode.get();
    }
});

Template.doctor.events({
  'click #getPromoCode' () {
    let $code = $('#code');
    if ($code.hasClass('active')) {
      $code.removeClass('active');
      $code.hide('fast');
    } else {
      Meteor.call('getPromoCode', function(err, value) {
        if (err) {
          return;
        }
        promoCode.set(value);
        $code.addClass('active');
        $code.show('fast');
      });
    }
  },
  'click #howItWorkslink' () {
    Modal.show('howItWorks');
  },
  'keyup #fieldSearch' (e) {
    fieldSearch.set(e.currentTarget.value);
  },
  'click .table-btn-watch' () {
    Blaze.remove(currentTemplate);
    let patient = Collection.Patient.findOne(this._id);
    currentTemplate = Blaze.renderWithData(Template.patient, patient, document.getElementById('template'));
  },
  'click #btn-tab-1' () {
    Blaze.remove(currentTemplate);
    currentTemplate = Blaze.render(Template.patientList, document.getElementById('template'));
  }
})
