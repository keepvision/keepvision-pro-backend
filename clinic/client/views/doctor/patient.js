let dateForSelect = new ReactiveVar(),
  showSpinner = new ReactiveVar(),
  typeGraphics = new ReactiveVar(),
  graprhicDailyActivity,
  graphicDiagnosis;

Template.patient.onRendered(function() {
  let patient = this.data;
  dateForSelect.set(moment());
  typeGraphics.set('weeks');


  this.autorun(() => {
    let dateSelect = dateForSelect.get(),
      aggrDataSubscr,
      weeks = [{
        year: moment(dateSelect).subtract(1, 'week').year(),
        number: moment(dateSelect).subtract(1, 'week').week()
      }, {
        year: dateSelect.year(),
        number: dateSelect.week()
      }],
      month = [{
        year: moment(dateSelect).subtract(2, 'month').year(),
        number: moment(dateSelect).subtract(2, 'month').month()
      }, {
        year: moment(dateSelect).subtract(1, 'month').year(),
        number: moment(dateSelect).subtract(1, 'month').month()
      }, {
        year: dateSelect.year(),
        number:dateSelect.month()
      }];

    if (typeGraphics.get() === 'weeks') {
      aggrDataSubscr = this.subscribe('AggregatedDataWeeks', patient._id, weeks);
    } else if (typeGraphics.get() === 'month') {
      aggrDataSubscr = this.subscribe('AggregatedDataMonth', patient._id, month);
    }

    // showSpinner.set(true);
    $("#dailyActivity").fadeTo(0, 0.1);
    if (aggrDataSubscr.ready()) {
      // showSpinner.set(false);
      $("#dailyActivity").fadeTo(0, 1);
      if (typeGraphics.get() === 'weeks') {
        drawTwoWeekActivity(weeks);
      } else if (typeGraphics.get() === 'month') {
        drawThreeMonthActivity(month);
      }
    }
  });

  this.autorun(() => {
    let dateSelect = dateForSelect.get().toDate(),
      diagnosisSubscr;
    if (typeGraphics.get() === 'weeks') {
      diagnosisSubscr = this.subscribe('DiagnosisWeeks', patient._id, dateSelect);
    } else if (typeGraphics.get() === 'month') {
      diagnosisSubscr = this.subscribe('DiagnosisMonth', patient._id, dateSelect);
    }
    if (diagnosisSubscr.ready()) {
      drawDiagnosis();
    }
  });
});

Template.patient.helpers({
  fio_patient() {
      return utilClient.getFullFIO(this);
    },
    age() {
      return this.birthdate && moment().diff(this.birthdate, 'years');
    },
    showSpinner() {
      return showSpinner.get();
    },
    day_active() {
      return typeGraphics.get() === 'weeks';
    }
});

Template.patient.events({
  'click #nextBtn' () {
    if (typeGraphics.get() === 'weeks') {
      dateForSelect.set(dateForSelect.get().add(1, 'week'));
    } else if (typeGraphics.get() === 'month') {
      dateForSelect.set(dateForSelect.get().add(1, 'month'));
    }
  },
  'click #prevBtn' () {
    if (typeGraphics.get() === 'weeks') {
      dateForSelect.set(dateForSelect.get().subtract(1, 'week'));
    } else if (typeGraphics.get() === 'month') {
      dateForSelect.set(dateForSelect.get().subtract(1, 'month'));
    }
  },
  'click #tabMonth' () {
    typeGraphics.set('month');
  },
  'click #tabWeeks' () {
    typeGraphics.set('weeks');
  }
});


function getDaysByWeeks(weeks) {
  let days = [];
  weeks.forEach(function(week) {
    let startWeek = moment().week(week.number).startOf('week'),
      endWeek = moment().week(week.number).endOf('week').add(1, 'days');
    while (startWeek.date() !== endWeek.date()) {
      days.push(moment(startWeek));
      startWeek.add(1, 'days');
    }
  });
  return days;
}

function getWeeksByMonths(months) {
  let weeks = [];
  let startWeek = moment().month(months[0].number).startOf('month');
  endWeek = moment().month(months[2].number).endOf('month');
  while (startWeek.week() !== endWeek.week()) {
    weeks.push(moment(startWeek));
    startWeek.add(1, 'week');
  }
  return weeks;
}


function drawThreeMonthActivity(months) {
  graprhicDailyActivity && graprhicDailyActivity.destroy();

  let ctx = document.getElementById("dailyActivity").getContext("2d"),
    labels = [],
    dataTotal = [],
    dataSafely = [],
    weeks = getWeeksByMonths(months);

  for (let i = 0; i < weeks.length; i++) {
    let week = weeks[i].week(),
        year = weeks[i].year();

    let agrDay = Collection.AggregatedData.findOne({
      'interval_date.week': week,
      'interval_date.year': year
    });
    if (agrDay) {
      dataTotal.push(agrDay.interval_data.total_minutes);
      dataSafely.push(agrDay.interval_data.total_minutes - agrDay.interval_data.threat_minutes);
    } else {
      dataTotal.push(0);
      dataSafely.push(0);
    }

    labels.push(`${weeks[i].startOf('week').format('D')}-${weeks[i].endOf('week').format('D MMM')}`);
  }

  let settings = {
    labels: labels,
    datasets: [{
      fillColor: "rgba(255,0,0,1)",
      // strokeColor: "#59cbff",
      pointColor: "rgba(255,0,0,1)",
      // pointStrokeColor: "#fff",
      // pointHighlightFill: "#fff",
      pointHighlightStroke: "rgba(255,0,0,1)",
      data: dataTotal
    }, {
      fillColor: "#d0f0ff",
      strokeColor: "#59cbff",
      pointColor: "rgba(56,143,221,1)",
      pointStrokeColor: "#fff",
      pointHighlightFill: "#fff",
      pointHighlightStroke: "rgba(56,143,221,1)",
      data: dataSafely
    }]
  };

  graprhicDailyActivity = new Chart(ctx).Line(settings, {});
}

function drawTwoWeekActivity(weeks) {
  graprhicDailyActivity && graprhicDailyActivity.destroy();

  let ctx = document.getElementById("dailyActivity").getContext("2d"),
    labels = [],
    dataTotal = [],
    dataSafely = [],
    days = getDaysByWeeks(weeks);
  for (let i = 0; i < days.length; i++) {
    let day = days[i];

    let agrDay = Collection.AggregatedData.findOne({
      'interval_date.day': day.date(),
      'interval_date.year': day.year()
    });
    if (agrDay) {
      dataTotal.push(agrDay.interval_data.total_minutes);
      dataSafely.push(agrDay.interval_data.total_minutes - agrDay.interval_data.threat_minutes);
    } else {
      dataTotal.push(0);
      dataSafely.push(0);
    }
    if (i === 0 || i === days.length - 1) { // первый и последний особо  
      labels.push(day.format('D MMMM'));
    } else {
      labels.push(day.date());
    }
  }

  let settings = {
    labels: labels,
    datasets: [{
      fillColor: "rgba(255,0,0,1)",
      // strokeColor: "#59cbff",
      pointColor: "rgba(255,0,0,1)",
      // pointStrokeColor: "#fff",
      // pointHighlightFill: "#fff",
      pointHighlightStroke: "rgba(255,0,0,1)",
      data: dataTotal
    }, {
      fillColor: "#d0f0ff",
      strokeColor: "#59cbff",
      pointColor: "rgba(56,143,221,1)",
      pointStrokeColor: "#fff",
      pointHighlightFill: "#fff",
      pointHighlightStroke: "rgba(56,143,221,1)",
      data: dataSafely
    }]
  };

  graprhicDailyActivity = new Chart(ctx).Line(settings, {});
}


function drawDiagnosis() {
  graphicDiagnosis && graphicDiagnosis.destroy();

  let ctx = document.getElementById("diagnosis").getContext("2d"),
    labels = [],
    dataLeft = [],
    dataRight = [],
    dataBoth = [];

  let diagnosis = Collection.Diagnosis.find().fetch();
  diagnosis = _.sortBy(diagnosis, function(diagnos) {
    return diagnos.date_update;
  });
  diagnosis.forEach(function(diagnos) {
    dataLeft.push(diagnos.diagnosis_data.testResult.left);
    dataRight.push(diagnos.diagnosis_data.testResult.right);
    dataBoth.push(diagnos.diagnosis_data.testResult.both);
    labels.push(moment(diagnos.date_update).format('DD.MM.YYYY'));
  });

  if (diagnosis.length === 1) {
    let diagnos = diagnosis[0];
    dataLeft.push(diagnos.diagnosis_data.testResult.left);
    dataRight.push(diagnos.diagnosis_data.testResult.right);
    dataBoth.push(diagnos.diagnosis_data.testResult.both);
    labels.push(moment(diagnos.date_update).format('DD.MM.YYYY'));
  }

  let settings = {
    labels: labels,
    datasets: [{
      fillColor: "red",
      pointColor: "red",
      strokeColor: "red",
      pointStrokeColor: "red",
      data: dataLeft
    }, {
      fillColor: "green",
      pointColor: "green",
      strokeColor: "green",
      pointStrokeColor: "green",
      data: dataRight
    }, {
      fillColor: "blue",
      pointColor: "blue",
      strokeColor: "blue",
      pointStrokeColor: "blue",
      data: dataBoth
    }]
  };
  graphicDiagnosis = new Chart(ctx).Line(settings, {
    datasetFill: false,
    scaleOverride: true,
    scaleStartValue: 0,
    scaleStepWidth: 0.2,
    scaleSteps: 6,
  });
}
