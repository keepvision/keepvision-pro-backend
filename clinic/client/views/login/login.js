var messageText = new ReactiveVar();
Template.authorization.onRendered(function() {
  messageText.set('');
});

Template.login.helpers({
  messageText: function() {
    return messageText.get();
  }
});

Template.login.events({
  'click #forgotPassBtn': function() {
    $('#forgotPassBlock').show();
    $('#authorizationBlock').hide();
  },
  'click .cancel': function() {
    $('#forgotPassBlock').hide();
    $('#authorizationBlock').show();
  },
  'submit #authorizationForm': function(e) {
    e.preventDefault();
    var $form = $(e.currentTarget),
      email = $form.find('[name = email]').val(),
      password = $form.find('[name = password]').val();
    Meteor.loginWithPassword(email, password, function(err) {
      if (err) {
        messageText.set(err.reason);
      } else {
        Modal.hide();
        utilClient.actionAfterAuthorization();
      }
    });
  },
  'submit #forgotPassForm': function(e) {
    e.preventDefault();
    var $form = $(e.currentTarget),
      email = $form.find('[name = email]').val();
    Accounts.forgotPassword({
      email: email
    }, function(err) {
      if (err) {
        messageText.set(err.reason);
      } else {
        messageText.set("На вашу почту  отправлено письмо с инструкциями по восстановлению пароля");
      }
    });
  },
});
