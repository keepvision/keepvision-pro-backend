utilClient = {
  actionAfterAuthorization() {
      let user = Meteor.user(),
        template;

      if ('manager' === user.roles[0]) {
        template = 'manager';
      } else if ('doctor' === user.roles[0]) {
        template = 'doctor';
      }

      Router.go(template);
    },
    getFullFIO(man) {
      let fio = '';
      if (man.last_name) {
        fio += man.last_name + ' ';
      }
      if (man.first_name) {
        fio += man.first_name + ' ';
      }
      if (man.mid_name) {
        fio += man.mid_name;
      }
      return fio;
    }
};
