Template.layout.helpers({
  clinic_name() {
    let clinic = Collection.Clinic.findOne();
    return clinic && clinic.name;
  }
});

Template.layout.events({
  'click #logout' () {
    Meteor.logout();
  },
  'click #changePassword' () {
  	Modal.show('passwordChangeDialog');
  }
})
